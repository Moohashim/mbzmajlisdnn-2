use [MBZMajlis]
GO
IF OBJECT_ID('dbo.tblRegistrationData', 'U') IS NOT NULL
 begin
   DROP TABLE [dbo].[tblRegistrationData];
 end    
go
  
CREATE TABLE [dbo].[tblRegistrationData](
        [PKey]                 [int] IDENTITY(1,1) NOT NULL,
        [UserId]               [int] null,

        [FirstName]            [nvarchar](250) not null,
        [MiddleName]           [nvarchar](250) not null,
        [FamilyName]           [nvarchar](250) not null,
        [Gender]               [nvarchar](50) not null,
        [Email]                [nvarchar](50) not null,
        [Emirate]              [nvarchar](250) null,
        [EndrolledUniversity]  [nvarchar](5) null,
        [Country]              [nvarchar](250) null, 
        [Institution]          [nvarchar](250) null, 
        [Level]                [nvarchar](250) null, 
        [StudyField]           [nvarchar](250) null, 
        [Birthday]             [date] null,

        [CreatedDate]          [datetime] NOT NULL DEFAULT getDATE(),
        [UpdatedDate]          [datetime] NULL DEFAULT getDATE(),
	
PRIMARY KEY CLUSTERED 
(
 [PKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) 
GO
print 'Create table tblRegistrationData Done'
go

GO
