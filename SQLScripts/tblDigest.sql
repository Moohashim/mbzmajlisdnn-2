
GO
IF OBJECT_ID('dbo.tblDigest', 'U') IS NOT NULL
 begin
   DROP TABLE [dbo].[tblDigest];
 end    
go
  
CREATE TABLE [dbo].[tblDigest](
        [PKey]                 [int] IDENTITY(1,1) NOT NULL,
        [UserId]               [int] NULL
	
PRIMARY KEY CLUSTERED 
(
 [PKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) 

GO
print 'Create table tblDigest Done'


insert into [dbo].[tblDigest] (UserId) values (1145)
--update [dbo].[tblDigest] set UserId = 1145

go

GO
