﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MBZMajlis
{
    public partial class StartPage : PortalModuleBase
	{
        public bool ShowDefaultPage
        {
            get { return base.Settings.ContainsKey("ControlKey") && (!string.IsNullOrEmpty(base.Settings["ControlKey"].ToString())); }
        }

        protected void RedirectModule(string strControlKey)
        {
            try
            {
                var mci = ModuleControlController.GetModuleControlByControlKey(strControlKey,this.ModuleConfiguration.ModuleDefID);

                var filetoload =@"~/" + mci.ControlSrc;
                var currentDir = this.AppRelativeTemplateSourceDirectory;
                PortalModuleBase objPortalModuleBase = (PortalModuleBase)LoadControl(filetoload);
                objPortalModuleBase.ID = System.IO.Path.GetFileNameWithoutExtension(mci.ControlSrc);

                this.ModuleConfiguration.ModuleTitle = mci.ControlTitle;
                objPortalModuleBase.ModuleConfiguration = ModuleConfiguration;
                Controls.Add(objPortalModuleBase);           

                //Control ctrl = this.ContainerControl.FindControl("dnnTITLE").FindControl("lblTitle");
                //if(ctrl is Label label)
                //    label.Text = mci.ControlTitle;
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }

        }
              
        protected void Page_Load(object sender, EventArgs e)
		{
            if (base.Settings.ContainsKey("ControlKey") && (!string.IsNullOrEmpty(base.Settings["ControlKey"].ToString())))
            {
                string controlKey=base.Settings["ControlKey"].ToString();
                RedirectModule(controlKey);
            }
		}
	}
}