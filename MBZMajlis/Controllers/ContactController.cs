﻿using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.UserRequest;
using DotNetNuke.Web.Api;
using MBZMajlis.Controllers.dto;
using MBZMajlis.dbContext;
using MBZMajlis.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Http;

namespace MBZMajlis.Controllers
{
    [DnnAuthorize]
    [AllowAnonymous]
    public class ContactController : ApiController
    {

        private DNNContext dbContext = new DNNContext();

        [HttpPost]
        public string Contact(dtoContactUsData data)
        {
                //var exist = dbContext.ContactUsData.Any(x => x.Email == data.Email);
                //if (exist)
                //{
                //   throw new Exception("User with the same email address has already sent an invitation");
                //}

                var ent = new entContactUsData()
                {
                    Email = data.Email,
                    Emirate = data.Emirate,
                    FullName = data.FullName,
                    PhoneNumber = data.PhoneNumber,
                    AddInfo = data.AddInfo,
                    CreatedDate = DateTime.Now
                };

                try
                {
                  dbContext.ContactUsData.Add(ent);
                  dbContext.SaveChanges();

                  EmailService.SendEmailContactAs(data);

                }
                catch(Exception ex)
                {
                     throw new Exception(ex.Message);
                }
          

                return "ok";
        }


       
    }
}