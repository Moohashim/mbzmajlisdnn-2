﻿using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.UserRequest;
using DotNetNuke.Web.Api;
using MBZMajlis.Controllers.dto;
using MBZMajlis.dbContext;
using MBZMajlis.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;

namespace MBZMajlis.Controllers
{
    [DnnAuthorize]
    public class AuthorizeUserController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage AuthorizeUser([FromBody] int userId)
        {
            bool res = new UserService().AuthorizeUser(userId);
            if (!res)
              return Request.CreateResponse(HttpStatusCode.Unauthorized);
         
           return Request.CreateResponse(HttpStatusCode.OK, "Success");

        }

        [HttpPost]
        public void AuthorizeUsers([FromBody] int[] users)
        {
            new UserService().AuthorizeUsers(users);
        }

    }
}