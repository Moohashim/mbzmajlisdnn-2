﻿
using DotNetNuke.Web.Api;

namespace MBZMajlis
{
    public class AppStartConfig : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute(
                "MBZMajlis",
                "default",
                "{controller}/{action}",
                new[] { "MBZMajlis.Controllers" });

           
        }
    }
}