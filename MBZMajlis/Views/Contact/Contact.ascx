﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contact.ascx.cs" Inherits="MBZMajlis.Views.Contact.Contact" %>

<% Func<string, string> GetString = (s) => DotNetNuke.Services.Localization.Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx"); %>

<div id="dnn_ctr400_HtmlModule_lblContent" class="Normal">
      <p class="contact-form-fullname">
          <input type="text" name="fullName" placeholder="<%= GetString("FullName") %>">
      </p>
     <p class="contact-form-email">
        <input type="email" name="mail" placeholder="<%= GetString("Email") %>">
     </p>
     <p class="contact-form-emirate">
        <input type="text" name="emirate" placeholder="<%= GetString("Emirate") %>">
     </p>
     <p class="contact-form-phone">
        <input type="tel" name="phone" placeholder="<%= GetString("PhoneNumber") %>">
     </p>
    <p class="border-none contact-form-additional"><span><%= GetString("AdditionalInformation") %></span></p>
    <textarea rows="5" name="additionalInfo"></textarea>
    
    <input type="button" value="<%= GetString("Contactus") %>" onclick="submitContactData()" id="contact-btn">
     <div class="form-response-output"> </div>
</div>


<script src='<%= ResolveUrl("../../Web/Contact/js/script.js") %>'></script>
    