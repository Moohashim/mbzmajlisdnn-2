﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Registration.ascx.cs" Inherits="MBZMajlis.Views.Registration.Registration" %>

<% Func<string, string> GetString = (s) => DotNetNuke.Services.Localization.Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx"); %>

       <div class="registration-form-container">
        <form class="registration-form">
            <p class="username-group">
                <input type="text" name="firstName" placeholder="<%= GetString("FirstName") %>" required="">
            </p>
            <p class="username-group">
                <input type="text" name="middleName" placeholder="<%= GetString("MiddleName") %>" required="">
            </p>
            <p class="username-group">
                <input type="text" name="familyName" placeholder="<%= GetString("FamilyName") %>" required="">
            </p>
            <p class="username-group">
                <select name="gender" required="">
                    <option value="male"><%= GetString("Male") %></option>
                    <option value="female"><%= GetString("Female") %></option>
                </select>
            </p>
            <p class="birthday-group">
                <input type="text" name="birthday" id="datepicker" placeholder="<%= GetString("DateOfBirthday") %>">
            </p>
            <p class="email-group">
                <input type="email" name="mail" placeholder="<%= GetString("Email") %>" required="">
            </p>
            <p class="emirate-group">
                <select name="emirate" required="">
                     <option value="Abu Dhabi"><%= GetString("AbuDhabi") %></option>
                    <option value="Ajman"><%= GetString("Ajman") %></option>
                    <option value="Dubai"><%= GetString("Dubai") %></option>
                    <option value="Fujairah"><%= GetString("Fujairah") %></option>
                    <option value="Ras al-Khaimah"><%= GetString("RasAlKhaimah") %></option>
                    <option value="Sharjah"><%= GetString("Sharjah") %></option>
                    <option value="Umm al-Quwain"><%= GetString("UmmAlQuwain") %></option>
                </select>
            </p>
            <p class="username-group">
                <select name="endrolled-university" required="">
                    <option value="" disabled="" selected=""><%= GetString("QuestionEnrollInUniversity") %></option>
                    <option value="yes"><%= GetString("Yes") %></option>
                    <option value="no"><%= GetString("No") %></option>
                </select>
            </p>
            <div class="hide-inputs" id="endrolled-university-yes">
                <p class="country-group">
                    <select name="country">
                        <option selected="" value="UAE"><%= GetString("UAE") %></option>
                        <option value="Outside UAE"><%= GetString("OutsideUAE") %></option>
                    </select>
                </p>
                <div id="selected-country-uae">
                    <p class="institue-group">
                        <select name="institution" id="institution" required="">
                           <option selected="" disabled="" value=""><%= GetString("Institution") %></option>
                        </select>
                    </p>
                    <p class="institue-group">
                        <select name="level" id="level" required="">
                            <option selected="" disabled="" value=""><%= GetString("Level") %></option>
                        </select>
                    </p>
                    <p class="institue-group">
                        <select name="study-field" id="study-field" required="">
                            <option selected="" disabled="" value=""><%= GetString("FieldOfStudy") %></option>
                        </select>
                    </p>
                </div>
                <div id="selected-country-outside" class="hide-inputs">
                    <p class="institue-group">
                        <input type="text" name="institution" placeholder="<%= GetString("Institution") %>">
                    </p>
                    <p class="institue-group">
                        <input type="text" name="level" placeholder="<%= GetString("Level") %>">
                    </p>
                    <p class="institue-group">
                        <input type="text" name="study" placeholder="Field of study">
                    </p>
                </div>
            </div>

            <input type="button" id="register-button" onclick="submitRegistartionData()" value="<%= GetString("Register") %>">
            <div class="form-response-output"></div>
        </form>
    </div>

    
    <script src='<%= ResolveUrl("../../Web/Registration/js/script.js") %>'></script>
    