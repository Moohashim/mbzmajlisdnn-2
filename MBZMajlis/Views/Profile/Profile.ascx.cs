﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using System;
using System.Web;
using System.Web.UI;

namespace MBZMajlis.Views.Profile
{
    public partial class Profile : PortalModuleBase
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicesFramework.Instance.RequestAjaxScriptSupport();

            UserInfo ui = DotNetNuke.Entities.Users.UserController.Instance.GetCurrentUserInfo();

            string userId = Convert.ToString(ui.UserID);
            if (ui.IsSuperUser)
            {
                userId = Request.QueryString["userId"];
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Profile", "getProfileData("+ userId + ");", true);
                        
        }


    }
}