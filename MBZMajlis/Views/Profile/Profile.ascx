﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="MBZMajlis.Views.Profile.Profile" %>            


<div class="registration-form-container registration-form">
                <div class="registration-form">
                    <div class="two-fields-group">
                        <p class="username-group">
                            <input type="text" name="firstName" placeholder="First Name" required="">
                        </p>
                        <p class="username-group">
                            <input type="text" name="middleName" placeholder="Middle Name" required="">
                        </p>
                    </div>
                    <div class="two-fields-group">
                        <p class="username-group">
                            <input type="text" name="familyName" placeholder="Family Name" required="">
                        </p>
                        <p class="username-group">
                            <select name="gender" required="">
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </p>

                    </div>
                    <div class="two-fields-group">
                        <p class="birthday-group">
                            <input type="text" name="birthday" id="datepicker" placeholder="Date of Birthday">
                        </p>
                        <p class="email-group">
                            <input type="email" name="mail" placeholder="Email" disabled="disabled">
                        </p>

                    </div>
                    <div class="two-fields-group">
                        <p class="emirate-group">
                            <select name="emirate" required="">
                                 <option value="Abu Dhabi">Abu Dhabi</option>
                                 <option value="Ajman">Ajman</option>
                                 <option value="Dubai">Dubai</option>
                                 <option value="Fujairah">Fujairah</option>
                                 <option value="Ras al-Khaimah">Ras al-Khaimah</option>
                                 <option value="Sharjah">Sharjah</option>
                                 <option value="Umm al-Quwain">Umm al-Quwain</option>
                            </select>
                        </p>
                        <p class="password-group">
                            <input type="text" name="password" placeholder="Set new password" required="">
                        </p>
                    </div>
                    <div class="one-field-group">
                        <p class="username-group">
                            <select name="endrolled-university" required="">
                                <option value="" disabled="" selected="">Are you currently enrolled in a University?</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </p>
                    </div>
                    <div class="" id="endrolled-university-yes">
                        <p class="country-group">
                            <select name="country">
                                <option selected="" value="UAE">UAE</option>
                                <option value="Outside UAE">Outside UAE</option>
                            </select>
                        </p>
                        <div id="selected-country-uae">
                            <p class="institue-group">
                                <select name="institution" id="institution" required="">
                                     <option selected="" disabled="" value="">Institution</option>
                                </select>
                            </p>
                            <p class="institue-group">
                                <select name="level" id="level" required="">
                                    <option selected="" disabled="" value="">Level</option>
                                </select>
                            </p>
                            <p class="institue-group">
                                <select name="study-field" id="study-field" required="">
                                    <option selected="" disabled="" value="">Field of study</option>
                                </select>
                            </p>
                        </div>
                        <div id="selected-country-outside">
                            <p class="institue-group">
                                <input type="text" name="institution" placeholder="Institution">
                            </p>
                            <p class="institue-group">
                                <input type="text" name="level" placeholder="Level">
                            </p>
                            <p class="institue-group">
                                <input type="text" name="study" placeholder="Field of study">
                            </p>
                        </div>
                    </div>

                    <input type="button" value="Save changes" onclick="submitProfileData()" id="edit-btn">
                    <div class="form-response-output"> </div>             
                </div>
 </div>

    
    <script src='<%= ResolveUrl("../../Web/Profile/js/script.js") %>'></script>
    