﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Framework;
using DotNetNuke.Services.UserRequest;
using MBZMajlis.dbContext;
using MBZMajlis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MBZMajlis.Views.Login
{
    public partial class Login : PortalModuleBase
    {
        private PortalController _portalController;

        protected PortalController navPortalController => _portalController ?? (_portalController = new PortalController());

        [Obsolete]
        private PortalSettings navPortalSettings => ServiceLocator<IPortalController, PortalController>.Instance.GetCurrentPortalSettings();

        Func<string, string> GetString = (s) => DotNetNuke.Services.Localization.Localization.GetString(s, "~/App_GlobalResources/MBZMajlis.resx");

        protected void Page_Load(object sender, EventArgs e)
        {
            ServicesFramework.Instance.RequestAjaxScriptSupport();

            //string resVal = MBZMajlis.Properties.Resources.ResourceManager.GetString("Wrong email or password");

            this.email.Attributes["placeholder"] = GetString("Email");
            this.password.Attributes["placeholder"] = GetString("Password");
            this.Button2.Text = GetString("Login");
        }

        protected void LogIn_Click(Object sender, EventArgs e)
        {
            if (this.Step1.Visible)
            {
                var ok = CheckPerson();
                
                if (ok)
                {
                    Send2FACode();
                }
            }
            else if (this.Step2.Visible)
            {
                LogIn();
            }
        }

        private void Send2FACode()
        {
            //using (DNNContext dbContext = new DNNContext())
            {
                //var user = dbContext.RegistrationData.SingleOrDefault(i => i.Email.ToLower() == this.email.Value.ToLower());
                //if (user != null)
                {
                    Random generator = new Random();
                    String r = generator.Next(0, 1000000).ToString("D6");

                    EmailService.SendEmail2FA(this.email.Value, r);
                    this.Session["VerificationCode"] = r;
                }
            }
        }

        private bool CheckPerson()
        {
            try
            {
                var email = this.email.Value;
                var password = this.password.Value;

                var portalIdName = GetPortalIdName(PortalSettings.Current.PortalId);
                var status = DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_FAILURE;

                var userInfo = UserController.GetUserByEmail(Convert.ToByte(PortalSettings.Current.PortalId), email);
                     
                var userRequestIpAddress = UserRequestIPAddressController.Instance.GetUserRequestIPAddress(new HttpRequestWrapper(HttpContext.Current.Request));
                var result = UserController.ValidateUser(portalIdName.Key, userInfo.Username, password, string.Empty, portalIdName.Value, userRequestIpAddress, ref status);

                if (status == DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_SUPERUSER)
                {
                    if (!LogIn(false))
                    {
                        throw new Exception(GetString("Wrong_email_or_password"));
                    }
                }

                bool ok = status == DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_SUCCESS || status == DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_SUPERUSER;

                if (!ok)
                {
                    throw new Exception(GetString("Wrong_email_or_password"));
                }
            }
            catch
            {
                this.lblError.Text = GetString("Wrong_email_or_password");
                return false;
            }

            this.Step1.Visible = false;
            this.Step2.Visible = true;

            return true;
        }

        private bool LogIn(bool validateCode = true)
        {
            try
            {
                if (validateCode && this.Session["VerificationCode"].ToString() != this.code.Value)
                {
                    throw new Exception(GetString("Wrong_verification_code"));
                }
                var email = this.email.Value;
                var password = this.password.Value;

                var portalIdName = GetPortalIdName(PortalSettings.Current.PortalId);
                var status = DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_FAILURE;

                var userInfo = UserController.GetUserByEmail(Convert.ToByte(PortalSettings.Current.PortalId), email);
                var userRequestIpAddress = UserRequestIPAddressController.Instance.GetUserRequestIPAddress(new HttpRequestWrapper(HttpContext.Current.Request));
                var result = UserController.UserLogin(portalIdName.Key, userInfo.Username, password, string.Empty, portalIdName.Value, userRequestIpAddress, ref status, false);

                bool ok = status == DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_SUCCESS || status == DotNetNuke.Security.Membership.UserLoginStatus.LOGIN_SUPERUSER;

                if (ok)
                {
                    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
                }
                else
                {
                    //MBZMajlis.Properties.Resources.Wrong_email_or_password
                    throw new Exception(GetString("Wrong_verification_code"));
                }
            }
            catch
            {
                this.lblError.Text = GetString("Wrong_verification_code");
                return false;
            }
            return true;
        }

      

        private KeyValuePair<int, string> GetPortalIdName(int? portalId = null)
        {
            if (!portalId.HasValue)
            {
                return new KeyValuePair<int, string>(navPortalSettings.PortalId, navPortalSettings.PortalName);
            }

            var portals = GetPortals().Where(i => i.Key == portalId.Value);
            if (portals.Count() != 1)
            {
                throw new Exception("Portal With PortalId = " + portalId.Value + " does not exist.");
            }

            return portals.Single();
        }

        public IQueryable<KeyValuePair<int, string>> GetPortals()
        {
            return navPortalController.GetPortalList("en-us")
                .Select(i => new KeyValuePair<int, string>
                (
                    i.PortalID,
                    i.PortalName
                ))
                .AsQueryable();
        }
    }
}