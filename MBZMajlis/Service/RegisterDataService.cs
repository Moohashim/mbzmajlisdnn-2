﻿using DotNetNuke.Entities.Users;
using MBZMajlis.dbContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MBZMajlis.Service
{
    public class RegisterDataService
    {
        private DNNContext dbContext = new DNNContext();


        public entRegistrationData GetByUserId(int id)
        {
            var ent = dbContext.RegistrationData.SingleOrDefault(x => x.UserId == id);

            return ent;
        }

        public void InsertData(entRegistrationData ent)
        {
            dbContext.RegistrationData.Add(ent);
            dbContext.SaveChanges();

        }

        public void UpdateData(entRegistrationData ent)
        {
            dbContext.RegistrationData.Attach(ent);
            dbContext.Entry(ent).State = EntityState.Modified;
            dbContext.SaveChanges();
        }


       

    }
}