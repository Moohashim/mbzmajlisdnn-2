<%@ Control language="cs" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>

<!--#include file="Common/Common.ascx"-->

<div class="container">
<!--#include file="Common/ProfileLink.ascx"-->
<!--#include file="Common/Menu.ascx"-->
    <main class="maincontent">


        <section class="hub-single-section jubilee">
            <div class="jubilee-content">
                <div class="jubilee-galleries">
                    <div class="section-title">
                        <h2><%= GetString("MBZMFGJubileeLab") %></h2>
                    </div>

                    <div id="ContentPane" runat="server" class="hub-gallery-wrapper three-items"></div>

                    <div class="section-title">
                        <h2><%= GetString("MBZMFGJubileeLabPhotos") %></h2>
                    </div>

                    <div id="JubileeLabPhotosPane" runat="server" class="hub-gallery-wrapper four-items photo-gallery"></div>

                    <div class="section-title">
                        <h2><%= GetString("MBZMFGJubileeLabVideos") %></h2>
                    </div>

                    <div id="JubileeLabVideosPane" runat="server" class="hub-gallery-wrapper four-items"></div>
                </div>
                <div class="jubilee-hub-image">
                    <img src="<%=SkinPath%>/assets/model4.png" alt="">
                </div>
            </div>

        </section>

        <section class="hub-single-section virtual-forum">

            <div class="section-title">
                <h2><%= GetString("VirtualLab2018Photos") %></h2>
            </div>

            <div id="JubileeLab2018PhotosPane" runat="server" class="hub-gallery-wrapper four-items photo-gallery"></div>

            <div class="section-title">
                <h2><%= GetString("VirtualLab2018Videos") %></h2>
            </div>

            <div id="JubileeLab2018VideosPane" runat="server" class="hub-gallery-wrapper four-items"></div>

        </section>

        <section class="hub-single-section survey">
            <div class="section-title">
                <h2><%= GetString("MBZMFGSurvey") %></h2>
            </div>

            <div id="SurveyPane" runat="server" class="hub-gallery-wrapper three-items"></div>

        </section>


        <section class="hub-single-section video-gallery-section">
            <div class="section-title">
                <h2><%= GetString("VIDEOGALLERY") %></h2>
            </div>

            <div id="VideoGalleryPane" runat="server" class="hub-gallery-wrapper four-items"></div>
        </section>


        <section class="hub-single-section photo-gallery-section">
            <div class="section-title">
                <h2><%= GetString("PHOTOGALLERY") %></h2>
            </div>

            <div id="PhotoGalleryPane" runat="server" class="hub-gallery-wrapper four-items photo-gallery"></div>
        </section>

    </main>
</div>
