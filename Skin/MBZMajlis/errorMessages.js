﻿
    const errorMessages = {
        registration: {
            formType: 'registration-form-content',
            error: {
                type: 'error',
                message: 'Registration failed! Something wrong!'
            }
        },
        login: {
            formType: 'login-form-content',
            error: {
                type: 'error',
                message: 'Something wrong!'
            }
        },
        contact: {
            formType: 'contact-form',
            success: {
                type: 'success',
                message: 'Message sent!'
            },
            error: {
                type: 'error',
                message: 'Message not sent!'
            }
        },
        edit: {
            formType: 'registration-form-content',
            success: {
                type: 'success',
                message: 'Your profile is updated!'
            },
            error: {
                type: 'error',
                message: 'Something wrong, your profile is not updated!'
            }
        }
    }
