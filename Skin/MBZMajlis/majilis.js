
    let institution = [
        {
            "institute": "ABU DHABI POLYTECHNIC",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "APPLIED BACHELOR IN AIRCRAFT MAINTENANCE MANAGEMENT",
                        "APPLIED BACHELOR IN ELECTROMECHANICAL ENGINEERING TECHNOLOGY",
                        "APPLIED BACHELOR IN INFORMATION SECURITY ENGINEERING TECHNOLOGY",
                        "APPLIED BACHELOR IN PETROLEUM ENGINEERING TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN METEOROLOGY"
                    ]
                },
                {
                    "level": "UNDERGRADUATE CERTIFICATE",
                    "fields": [
                        "CERTIFICATE IN AIRCRAFT MAINTENANCE"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN AIR TRAFFIC MANAGEMENT",
                        "DIPLOMA IN AIRCRAFT ENGINEERING TECHNOLOGIES",
                        "DIPLOMA IN AIRCRAFT MAINTENANCE TECHNOLOGY",
                        "DIPLOMA IN ELECTROMECHANICAL ENGINEERING",
                        "DIPLOMA IN METEOROLOGY",
                        "DIPLOMA IN OIL AND GAS PROCESS ENGINEERING"
                    ]
                },
                {
                    "level": "UNDERGRADUATE HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN AIR TRAFFIC MANAGEMENT",
                        "HIGHER DIPLOMA IN AIRCRAFT ENGINEERING TECHNOLOGY \u2013 AEROMECHANIC",
                        "HIGHER DIPLOMA IN AIRCRAFT ENGINEERING TECHNOLOGY \u2013 AVIONIC",
                        "HIGHER DIPLOMA IN AIRCRAFT MAINTENANCE TECHNOLOGY \u2013 AEROMECHANICS",
                        "HIGHER DIPLOMA IN AIRCRAFT MAINTENANCE TECHNOLOGY \u2013 AVIONICS",
                        "HIGHER DIPLOMA IN ELECTROMECHANICAL ENGINEERING TECHNOLOGY",
                        "HIGHER DIPLOMA IN INFORMATION SECURITY ENGINEERING TECHNOLOGY",
                        "HIGHER DIPLOMA IN METEOROLOGY",
                        "HIGHER DIPLOMA IN NUCLEAR TECHNOLOGY",
                        "HIGHER DIPLOMA IN PETROLEUM ENGINEERING TECHNOLOGY",
                        "HIGHER DIPLOMA IN SEMICONDUCTOR TECHNOLOGY"
                    ]
                }
            ]
        },
        {
            "institute": "ABU DHABI SCHOOL OF MANAGEMENT",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF SCIENCE IN BUSINESS ANALYTICS"
                    ]
                }
            ]
        },
        {
            "institute": "ABU DHABI UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN PERSIAN LANGUAGE",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN ACCOUNTING",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN DIGITAL MARKETING COMMUNICATIONS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN FINANCE",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN HUMAN RESOURCES MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MANAGEMENT",
                        "BACHELOR OF LAW",
                        "BACHELOR OF MASS COMMUNICATION (ARABIC)",
                        "BACHELOR OF MILITARY SCIENCES AND ADMINISTRATION",
                        "BACHELOR OF MILITARY SCIENCES AND MANAGEMENT",
                        "BACHELOR OF SCIENCE IN AVIATION",
                        "BACHELOR OF SCIENCE IN BIOMEDICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN BIOMEDICAL SCIENCE: LABORATORY MEDICINE",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN CYBERSECURITY ENGINEERING",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL HEALTH AND SAFETY",
                        "BACHELOR OF SCIENCE IN HUMAN NUTRITION AND DIETETICS",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN INTERIOR DESIGN",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MOLECULAR AND MEDICAL GENETICS",
                        "BACHELOR OF SCIENCE IN PUBLIC HEALTH",
                        "BACHELOR OF SCIENCE IN SOFTWARE ENGINEERING"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTORATE OF BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF EDUCATION IN EDUCATIONAL LEADERSHIP",
                        "MASTER OF ENGINEERING IN ELECTRICAL AND COMPUTER ENGINEERING",
                        "MASTER OF ENGINEERING MANAGEMENT",
                        "MASTER OF HUMAN RESOURCES MANAGEMENT",
                        "MASTER OF HUMAN RESOURCES MANAGEMENT(in Arabic for Military)",
                        "MASTER OF INTERNATIONAL RELATIONS",
                        "MASTER OF PRIVATE LAW",
                        "MASTER OF PROJECT MANAGEMENT",
                        "MASTER OF PUBLIC LAW",
                        "MASTER OF SCIENCE IN CIVIL ENGINEERING",
                        "MASTER OF SCIENCE IN ELECTRICAL AND COMPUTER ENGINEERING",
                        "MASTER OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "MASTER OF SCIENCE IN MECHANICAL ENGINEERING",
                        "MASTER OF SCIENCE IN SPECIAL EDUCATION",
                        "MASTER OF SCIENCE IN SUSTAINABLE ARCHITECTURE",
                        "MASTER OF STRATEGIC LEADERSHIP"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL DIPLOMA IN TEACHING (ENGLISH)"
                    ]
                }
            ]
        },
        {
            "institute": "ABU DHABI VOCATIONAL EDUCATION AND TRAINING INSTITUTE",
            "levels": [
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN ACCOUNTING",
                        "DIPLOMA IN CAR MECHATRONICS",
                        "DIPLOMA IN DESIGN (FASHION)",
                        "DIPLOMA IN DESIGN (JEWELLERY)",
                        "DIPLOMA IN EVENT MANAGEMENT",
                        "DIPLOMA IN INDUSTRIAL MECHATRONICS",
                        "DIPLOMA IN INFORMATION TECHNOLOGY SYSTEMS (FIELD AND NETWORK SUPPORT)",
                        "DIPLOMA IN LOGISTICS AND MANAGEMENT",
                        "DIPLOMA IN MEDIA TECHNOLOGY",
                        "DIPLOMA IN PROCESS AUTOMATION",
                        "DIPLOMA IN TECHNICAL DRAWING",
                        "DIPLOMA IN TECHNICAL LABORATORY ANALYTICS",
                        "DIPLOMA IN TOURISM MANAGEMENT",
                        "DIPLOMA IN TRAVEL AND TOURISM MANAGEMENT",
                        "DIPLOMA OF ENGINEERING TECHNOLOGY- ELECTRICAL MAINTENANCE",
                        "DIPLOMA OF ENGINEERING TECHNOLOGY- MECHANICAL MAINTENANCE",
                        "DIPLOMA OF ENVIRONMENT, HEALTH AND SAFETY",
                        "DIPLOMA OF GRAPHIC DESIGN",
                        "DIPLOMA OF HUMAN RESOURCE MANAGEMENT",
                        "DIPLOMA OF INFORMATION TECHNOLOGY - MULTIMEDIA",
                        "DIPLOMA OF INFORMATION TECHNOLOGY - NETWORKING",
                        "DIPLOMA OF INTERIOR DESIGN AND DECORATION",
                        "DIPLOMA OF LIBRARY AND INFORMATION SERVICES",
                        "DIPLOMA OF PROJECT MANAGEMENT"
                    ]
                },
                {
                    "level": "UNDERGRADUATE HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN CIVIL ENGINEERING",
                        "HIGHER DIPLOMA IN ELECTRICAL AND ELECTRONICS ENGINEERING",
                        "HIGHER DIPLOMA IN INFORMATION TECHNOLOGY",
                        "HIGHER DIPLOMA IN LOGISTICS AND MANAGEMENT",
                        "HIGHER DIPLOMA IN MECHANICAL ENGINEERING"
                    ]
                }
            ]
        },
        {
            "institute": "AJMAN UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN ENGLISH LANGUAGE AND TRANSLATION",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF ARTS IN SOCIOLOGY AND SOCIAL WORK",
                        "BACHELOR OF EDUCATION IN TEACHER TRAINING PROGRAM IN ARABIC LANGUAGE AND ISLAMIC STUDIES",
                        "BACHELOR OF EDUCATION IN TEACHER TRAINING PROGRAM IN MATHEMATICS AND SCIENCE",
                        "BACHELOR OF EDUCATION- TEACHING ENGLISH AS A FOREIGN LANGUAGE",
                        "BACHELOR OF INTERIOR DESIGN",
                        "BACHELOR OF LAW",
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF SCIENCE IN ACCOUNTING",
                        "BACHELOR OF SCIENCE IN BIOMEDICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN BUILDING ENGINEERING AND CONSTRUCTION MANAGEMENT",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN DATA ANALYTICS",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN FINANCE",
                        "BACHELOR OF SCIENCE IN INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN MANAGEMENT",
                        "BACHELOR OF SCIENCE IN MARKETING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "DOCTOR OF DENTAL SURGERY"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF BUSINESS ADMINISTRATION",
                        "DOCTOR OF PHILOSOPHY IN LAW"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "MASTER OF ARTS IN PUBLIC RELATIONS AND CORPORATE COMMUNICATION",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF LAW IN PRIVATE LAW",
                        "MASTER OF LAW IN PUBLIC LAW",
                        "MASTER OF SCIENCE IN ARTIFICIAL INTELLIGENCE",
                        "MASTER OF SCIENCE IN ENDODONTICS",
                        "MASTER OF SCIENCE IN PEDIATRIC DENTISTRY",
                        "MASTER OF SCIENCE IN PHARMACY",
                        "MASTER OF SCIENCE IN RESTORATIVE DENTISTRY",
                        "MASTER OF SCIENCE IN URBAN DESIGN"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL DIPLOMA IN TEACHING"
                    ]
                }
            ]
        },
        {
            "institute": "AL AIN UNIVERSITY (PREVIOUSLY, AL AIN UNIVERSITY OF SCIENCE & TECHNOLOGY)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR IN BUSINESS ADMINISTRATION IN FINANCE AND BANKING",
                        "BACHELOR IN ISLAMIC STUDIES",
                        "BACHELOR OF ARTS IN APPLIED PSYCHOLOGY",
                        "BACHELOR OF ARTS IN APPLIED SOCIOLOGY",
                        "BACHELOR OF ARTS IN ENGLISH LANGUAGE AND TRANSLATION",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN ACCOUNTING",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MANAGEMENT INFORMATION SYSTEMS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MARKETING",
                        "BACHELOR OF EDUCATION IN SPECIAL EDUCATION",
                        "BACHELOR OF LAW",
                        "BACHELOR OF MASS COMMUNICATION AND MEDIA",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN CYBERSECURITY",
                        "BACHELOR OF SCIENCE IN NETWORKS AND COMMUNICATION ENGINEERING",
                        "BACHELOR OF SCIENCE IN NUTRITION AND DIETETICS",
                        "BACHELOR OF SCIENCE IN PHARMACY",
                        "BACHELOR OF SCIENCE IN SOFTWARE ENGINEERING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF ARTS IN TEACHING ENGLISH TO SPEAKERS OF OTHER LANGUAGES",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF CRIMINAL SCIENCE",
                        "MASTER OF EDUCATION IN ARABIC LANGUAGE CURRICULA AND INSTRUCTION",
                        "MASTER OF EDUCATION IN ISLAMIC EDUCATION CURRICULA AND INSTRUCTION",
                        "MASTER OF PRIVATE LAW",
                        "MASTER OF PUBLIC LAW",
                        "MASTER OF SCIENCE IN CLINICAL PHARMACY",
                        "MASTER OF SCIENCE IN PHARMACEUTICAL SCIENCES"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE PROFESSIONAL DIPLOMA IN TEACHING"
                    ]
                }
            ]
        },
        {
            "institute": "AL FALAH UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN ISLAMIC STUDIES",
                        "BACHELOR OF LAW",
                        "BACHELOR OF MASS COMMUNICATION",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "AL GHURAIR UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN INTERIOR DESIGN",
                        "BACHELOR OF ARTS IN PUBLIC RELATIONS",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF LAW",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE AND ENGINEERING",
                        "BACHELOR OF SCIENCE IN ELECTRICAL AND ELECTRONICS ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATIION",
                        "MASTER OF PRIVATE LAW",
                        "MASTER OF PUBLIC LAW",
                        "MASTER OF SCIENCE IN INFORMATION TECHNOLOGY MANAGEMENT",
                        "MASTER OF SCIENCE IN INTERNATIONAL BUSINESS"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL DIPLOMA IN JOURNALISM"
                    ]
                }
            ]
        },
        {
            "institute": "AL KHAWARIZMI INTERNATIONAL COLLEGE",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE IN HEALTH MANAGEMENT",
                        "ASSOCIATE IN MEDICAL LABORATORY ANALYSIS",
                        "ASSOCIATE IN MEDICAL RECORDS",
                        "ASSOCIATE OF COMPUTER GRAPHICS AND ANIMATION",
                        "ASSOCIATE OF FASHION DESIGN",
                        "ASSOCIATE OF SCIENCE IN BUSINESS ADMINISTRATION",
                        "ASSOCIATE OF SCIENCE IN ISLAMIC BANKING"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN HEALTH MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF ISLAMIC BANKING AND \u200EFINANCE \u200E",
                        "BACHELOR OF MASS COMMUNICATION",
                        "BACHELOR OF SCIENCE IN EMERGENCY MEDICAL CARE",
                        "BACHELOR OF SCIENCE IN MEDICAL LABORATORY ANALYSIS",
                        "BACHELOR OF SCIENCE IN RESPIRATORY CARE"
                    ]
                }
            ]
        },
        {
            "institute": "AL QASIMIYA UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN HOLY QURAN SCIENCES",
                        "BACHELOR OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN ECONOMICS",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF SHARIAH AND ISLAMIC STUDIES"
                    ]
                }
            ]
        },
        {
            "institute": "AL WASL UNIVERSITY (FORMERLY: ISLAMIC AND ARABIC STUDIES COLLEGE - DUBAI)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR IN ISLAMIC STUDIES",
                        "BACHELOR IN LIBRARY AND INFORMATION SCIENCES"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTORATE OF ARABIC LANGUAGE AND LITERATURE",
                        "DOCTORATE OF ISLAMIC SHARIAH"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF ARABIC LANGUAGE AND LITERATURE",
                        "MASTER OF ISLAMIC SHARIAH"
                    ]
                }
            ]
        },
        {
            "institute": "AMERICAN UNIVERSITY IN DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN INTERNATIONAL STUDIES",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF COMMUNICATION & INFORMATION STUDIES",
                        "BACHELOR OF FINE ARTS IN INTERIOR DESIGN",
                        "BACHELOR OF FINE ARTS IN VISUAL COMMUNICATION",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "UNDERGRADUATE CERTIFICATE",
                    "fields": [
                        "CERTIFICATE IN MIDDLE EASTERN STUDIES"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF ARTS IN INTELLECTUAL PROPERTY AND INNOVATION MANAGEMENT",
                        "MASTER OF ARTS IN INTERNATIONAL AFFAIRS",
                        "MASTER OF ARTS IN LEADERSHIP AND INNOVATION IN CONTEMPORARY MEDIA",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF EDUCATION",
                        "MASTER OF SCIENCE IN CONSTRUCTION MANAGEMENT",
                        "MASTER OF URBAN DESIGN AND DIGITAL ENVIRONMENTS"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL POSTGRADUATE DIPLOMA IN GOVERNMENT PERFORMANCE MANAGEMENT",
                        "PROFESSIONAL POSTGRADUATE DIPLOMA IN GOVERNMENT SERVICES",
                        "PROFESSIONAL TEACHING CERTIFICATE"
                    ]
                }
            ]
        },
        {
            "institute": "AMERICAN UNIVERSITY IN THE EMIRATES",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN MEDIA AND MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN SECURITY AND STRATEGIC STUDIES",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF EDUCATION",
                        "BACHELOR OF LAW",
                        "BACHELOR OF PUBLIC RELATIONS",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN DESIGN - DIGITAL ANIMATION",
                        "BACHELOR OF SCIENCE IN DESIGN - GRAPHIC DESIGN",
                        "BACHELOR OF SCIENCE IN DESIGN- FASHION DESIGN",
                        "BACHELOR OF SCIENCE IN DESIGN- INTERIOR DESIGN",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY MANAGEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN ARBITRATION",
                        "MASTER IN CRIMINAL SCIENCES",
                        "MASTER IN INTELLECTUAL PROPERTY",
                        "MASTER IN SECURITY STUDIES AND INFORMATION ANALYSIS",
                        "MASTER OF ARTS IN DIPLOMACY",
                        "MASTER OF ARTS IN SECURITY AND STRATEGIC STUDIES",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF KNOWLEDGE MANAGEMENT",
                        "MASTER OF SPORTS MANAGEMENT",
                        "PROFESSIONAL MASTER IN SPORTS LAW"
                    ]
                }
            ]
        },
        {
            "institute": "AMERICAN UNIVERSITY OF RAS AL KHAIMAH",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ART IN INTERIOR DESIGN",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF SCIENCE IN ARTIFICIAL INTELLIGENCE",
                        "BACHELOR OF SCIENCE IN BIOTECHNOLOGY",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION (MAJOR IN ACCOUNTING)",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION (MAJOR IN FINANCE)",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION (MAJOR IN HUMAN RESOURCE MANAGEMENT)",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION (MAJOR IN MARKETING)",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CIVIL AND INFRASTRUCTURE ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN ELECTRONICS AND COMMUNICATIONS ENGINEERING",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN PETROLEUM ENGINEERING",
                        "BAHELOR OF SCIENCE IN COMPUTER SCIENCE"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER IN INFRASTRUCTURE ENGINEERING",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF EDUCATION IN EDUCATIONAL LEADERSHIP",
                        "MASTER OF SCIENCE IN ENGINEERING PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE IN SUSTAINABLE AND RENEWABLE ENERGY"
                    ]
                }
            ]
        },
        {
            "institute": "AMERICAN UNIVERSITY OF SHARJAH",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN ECONOMICS",
                        "BACHELOR OF ARTS IN ENGLISH LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN INTERNATIONAL STUDIES",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF INTERIOR DESIGN",
                        "BACHELOR OF SCIENCE IN BIOLOGY",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - ACCOUNTING",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - ECONOMICS",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - FINANCE",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - MANAGEMENT",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - MANAGEMENT INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - MARKETING",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN DESIGN MANAGEMENT",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL SCIENCES",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MATHEMATICS",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MULTI MEDIA DESIGN",
                        "BACHELOR OF SCIENCE IN PHYSICS",
                        "BACHELOR OF SCIENCE IN VISUAL COMMUNICATION"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN BIOSCIENCES AND BIOENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN BUSINESS ADMINISTRATION",
                        "DOCTOR OF PHILOSOPHY IN ENGINEERING- ENGINEERING SYSTEMS MANAGEMENT",
                        "DOCTOR OF PHILOSOPHY IN MATERIALS SCIENCE AND ENGINEERING"
                    ]
                },
                {
                    "level": "GRADUATE CERTIFICATE",
                    "fields": [
                        "GRADUATE CERTIFICATE IN MUSEUM AND HERITAGE STUDIES"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "GULF EXECUTIVE MASTER OF PUBLIC ADMINISTRATION",
                        "MASTER OF ARTS IN ENGLISH/ARABIC/ENGLISH TRANSLATION AND INTERPRETING",
                        "MASTER OF ARTS IN TEACHING ENGLISH - SPEAKERS OF OTHER LANGUAGES (TESOL)",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF SCIENCE IN ACCOUNTING",
                        "MASTER OF SCIENCE IN BIOMEDICAL ENGINEERING",
                        "MASTER OF SCIENCE IN CHEMICAL ENGINEERING",
                        "MASTER OF SCIENCE IN CIVIL ENGINEERING",
                        "MASTER OF SCIENCE IN COMPUTER ENGINEERING",
                        "MASTER OF SCIENCE IN CONSTRUCTION MANAGEMENT",
                        "MASTER OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "MASTER OF SCIENCE IN ENGINEERING SYSTEMS MANAGEMENT",
                        "MASTER OF SCIENCE IN FINANCE",
                        "MASTER OF SCIENCE IN MATHEMATICS",
                        "MASTER OF SCIENCE IN MECHANICAL ENGINEERING",
                        "MASTER OF SCIENCE IN MECHATRONICS ENGINEERING",
                        "MASTER OF URBAN PLANNING"
                    ]
                }
            ]
        },
        {
            "institute": "ARAB ACADEMY FOR SCIENCE, TECHNOLOGY AND MARITIME TRANSPORT",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF MARINE ENGINEERING TECHNOLOGY",
                        "BACHELOR OF MARITIME TRANSPORT TECHNOLOGY"
                    ]
                }
            ]
        },
        {
            "institute": "BRITISH UNIVERSITY IN DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF LAW",
                        "BACHELOR OF SCIENCE IN ACCOUNTING AND FINANCE",
                        "BACHELOR OF SCIENCE IN ARCHITECTURE",
                        "BACHELOR OF SCIENCE IN BUSINESS MANAGEMENT",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN ELECTRO-\u200EMECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF EDUCATION",
                        "DOCTOR OF PHILISOPHY IN ARCHITECTURE AND SUSTAINABLE BUILT ENVIRONMENT",
                        "DOCTOR OF PHILOSOPHY IN BUSINESS LAW",
                        "DOCTOR OF PHILOSOPHY IN BUSINESS MANAGEMENT",
                        "DOCTOR OF PHILOSOPHY IN COMPUTER SCIENCE",
                        "DOCTOR OF PHILOSOPHY IN EDUCATION",
                        "DOCTOR OF PHILOSOPHY IN PROJECT MANAGEMENT",
                        "PROFESSIONAL DOCTORATE IN BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTERS",
                    "fields": [
                        "MASTER OF ARCHITECTURE",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF EDUCATION",
                        "MASTER OF EDUCATION IN INFORMATION AND COMMUNICATION TECHNOLOGY",
                        "MASTER OF EDUCATION IN LEARNING AND TEACHING",
                        "MASTER OF EDUCATION IN MANAGEMENT LEADERSHIP AND POLICY",
                        "MASTER OF EDUCATION IN PSYCHOLOGY",
                        "MASTER OF EDUCATION IN SCIENCE EDUCATION",
                        "MASTER OF EDUCATION IN SPECIAL AND INCLUSIVE EDUCATION",
                        "MASTER OF EDUCATION IN TEACHING ENGLISH TO SPEAKERS OF OTHER LANGUAGES (TESOL)",
                        "MASTER OF SCIENCE IN BUILDING SERVICES ENGINEERING",
                        "MASTER OF SCIENCE IN CONSTRUCTION LAW AND DISPUTE RESOLUTION",
                        "MASTER OF SCIENCE IN CONSTRUCTION PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE IN ENGINEERING MANAGEMENT",
                        "MASTER OF SCIENCE IN ENTERPRISE PROJECT RISK MANAGEMENT",
                        "MASTER OF SCIENCE IN FINANCE",
                        "MASTER OF SCIENCE IN INFORMATICS",
                        "MASTER OF SCIENCE IN INFORMATION TECHNOLOGY MANAGEMENT",
                        "MASTER OF SCIENCE IN INFORMATION TECHNOLOGY PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE IN INFRASTRUCTURE PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE IN PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE IN STRUCTURAL ENGINEERING",
                        "MASTER OF SCIENCE IN SUSTAINABLE DESIGN OF THE BUILT ENVIRONMENT"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN BUILDING SERVICES ENGINEERING",
                        "POSTGRADUATE DIPLOMA IN CONSTRUCTION LAW AND DISPUTE RESOLUTION",
                        "POSTGRADUATE DIPLOMA IN CONSTRUCTION PROJECT MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN EDUCATION",
                        "POSTGRADUATE DIPLOMA IN ENGINEERING MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN ENTERPRISE PROJECT RISK MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN FINANCE",
                        "POSTGRADUATE DIPLOMA IN INFORMATICS",
                        "POSTGRADUATE DIPLOMA IN INFORMATION AND COMMUNICATION TECHNOLOGY",
                        "POSTGRADUATE DIPLOMA IN INFORMATION TECHNOLOGY PROJECT MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN INFRASTRUCTURE PROJECT MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN LEARNING AND TEACHING",
                        "POSTGRADUATE DIPLOMA IN MANAGEMENT LEADERSHIP AND POLICY",
                        "POSTGRADUATE DIPLOMA IN PROJECT MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN PSYCHOLOGY",
                        "POSTGRADUATE DIPLOMA IN SCIENCE EDUCATION",
                        "POSTGRADUATE DIPLOMA IN SPECIAL AND INCLUSIVE EDUCATION",
                        "POSTGRADUATE DIPLOMA IN STRUCTURAL ENGINEERING",
                        "POSTGRADUATE DIPLOMA IN SUSTAINABLE DESIGN OF THE BUILT ENVIRONMENT",
                        "POSTGRADUATE DIPLOMA IN TEACHING ENGLISH TO SPEAKERS OF OTHER LANGUAGES (TESOL)",
                        "PROFESSIONAL GRADUATE DIPLOMA IN EDUCATION"
                    ]
                }
            ]
        },
        {
            "institute": "CANADIAN UNIVERSITY DUBAI",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE OF MARKETING"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN APPLIED SOCIOLOGY (ARABIC)",
                        "BACHELOR OF ARTS IN COMMUNICATION (ARABIC)",
                        "BACHELOR OF ARTS IN COMMUNICATION (ENGLISH)",
                        "BACHELOR OF ARTS IN CREATIVE INDUSTRIES",
                        "BACHELOR OF ARTS IN ENGLISH LANGUAGE AND TRANSLATION",
                        "BACHELOR OF ARTS IN PSYCHOLOGY (ARABIC)",
                        "BACHELOR OF ARTS IN PSYCHOLOGY (ENGLISH)",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF COMPUTER AND NETWORKING ENGINEERING TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN CYBER SECURITY",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL HEALTH MANAGEMENT",
                        "BACHELOR OF SCIENCE IN HEALTH INFORMATION MANAGEMENT",
                        "BACHELOR OF SCIENCE IN HEALTH ORGANIZATION MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INTERIOR DESIGN",
                        "BACHELOR OF SCIENCE IN NETWORK ENGINEERING",
                        "BACHELOR OF SCIENCE IN SOFTWARE DESIGN",
                        "BACHELOR OF TOURISM STUDIES"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN FOOD SAFETY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN INFORMATION TECHNOLOGY MANAGEMENT AND GOVERNANCE",
                        "MASTER OF BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "CITY UNIVERSITY COLLEGE OF AJMAN",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF BUSINESS ADMINISTRATION- HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF DENTAL SURGERY",
                        "BACHELOR OF LAW",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF PUBLIC RELATIONS AND ADVERTISEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN PRIVATE LAW",
                        "MASTER IN PUBLIC LAW",
                        "MASTER OF BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL DIPLOMA IN TEACHING"
                    ]
                }
            ]
        },
        {
            "institute": "DUBAI INSTITUTE OF DESIGN AND INNOVATION",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF DESIGN"
                    ]
                }
            ]
        },
        {
            "institute": "DUBAI MEDICAL COLLEGE FOR GIRLS",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN ADDICTION SCIENCE"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN ADDICTION SCIENCE"
                    ]
                }
            ]
        },
        {
            "institute": "DUBAI PHARMACY COLLEGE FOR GIRLS",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF PHARMACY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF PHARMACY"
                    ]
                }
            ]
        },
        {
            "institute": "DUBAI POLICE ACADEMY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN LAW",
                        "BACHELOR OF SECURITY AND CRIMINAL SCIENCES"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF SECURITY CRISIS MANAGEMENT",
                        "DOCTOR OF CRIMINAL LAW",
                        "DOCTOR OF PHILOSOPHY IN CRIMINAL INVESTIGATION",
                        "DOCTOR OF PRIVATE LAW",
                        "DOCTOR OF PUBLIC LAW"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN POLICE SCIENCES",
                        "HIGHER DIPLOMA IN GENERAL LAW",
                        "HIGHER DIPLOMA IN PRIVATE LAW",
                        "POSTGRADUATE DIPLOMA IN CRIMINAL SCIENCES",
                        "POSTGRADUATE DIPLOMA IN POLICE SCIENCES - CRIMINAL INVESTIGATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN SECURITY CRISIS MANAGEMENT",
                        "MASTER OF CRIMINAL SCIENCES",
                        "MASTER OF HUMAN RIGHTS",
                        "MASTER OF LAW AND ENVIRONMENT",
                        "MASTER OF LAW IN INTERNATIONAL INVESTMENTS AND COMMERCIAL LAW",
                        "MASTER OF POLICE SCIENCES IN CRIMINAL INVESTIGATION",
                        "MASTER OF PRIVATE LAW",
                        "MASTER OF PUBLIC LAW",
                        "MASTER IN INTELLECTUAL PROPERTY LAW"
                    ]
                },
                {
                    "level": "LICENSE",
                    "fields": [
                        "LICENSE IN LAW AND POLICE SCIENCES"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES ACADEMY FOR IDENTITY & CITIZENSHIP (FORMERLY: EMIRATES INSTITUTE FOR CITIZENSHIP AND RESIDENCE)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF CITIZENSHIP, RESIDENCE, AND PORTS"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN CITIZENSHIP, RESIDENCE, AND PORTS"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES ACADEMY OF HOSPITALITY MANAGEMENT",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE OF BUSINESS ADMINISTRATION IN INTERNATIONAL HOSPITALITY MANAGEMENT"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION (HONORS) IN INTERNATIONAL HOSPITALITY MANAGEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION IN INTERNATIONAL HOSPITALITY MANAGEMENT"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES AVIATION UNIVERSITY",
            "levels": [
                {
                    "level": "HIGHER DIPLOMA",
                    "fields": [
                        "ADVANCED DIPLOMA IN AEROSPACE ENGINEERING",
                        "ADVANCED DIPLOMA IN AIRCRAFT MAINTENANCE ENGINEERING",
                        "ADVANCED DIPLOMA IN AVIONICS ENGINEERING",
                        "ADVANCED DIPLOMA IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "APPLIED BACHELOR IN AEROSPACE ENGINEERING",
                        "APPLIED BACHELOR IN AIRCRAFT MAINTENANCE ENGINEERING",
                        "APPLIED BACHELOR IN AVIONICS ENGINEERING",
                        "APPLIED BACHELOR IN MECHANICAL ENGINEERING",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF SCIENCE IN AERONAUTICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN SOFTWARE ENGINEERING"
                    ]
                },
                {
                    "level": "DIPLOMA",
                    "fields": [
                        "DIPLOMA IN AEROSPACE ENGINEERING",
                        "DIPLOMA IN AIRCRAFT MAINTENANCE ENGINEERING",
                        "DIPLOMA IN AVIATION MANAGEMENT",
                        "DIPLOMA IN AVIONICS ENGINEERING",
                        "DIPLOMA IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "UNDERGRADUATE HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN AVIATION MANAGEMENT",
                        "HIGHER DIPLOMA IN BUSINESS MANAGEMENT",
                        "HIGHER DIPLOMA IN COMPUTING SOFTWARE DEVELOPMENT",
                        "HIGHER DIPLOMA IN TRAVEL AND TOURISM MANAGEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF ARTS IN INTERNATIONAL HUMAN RESOURCE MANAGEMENT",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF SCIENCE IN AEROSPACE ENGINEERING",
                        "MASTER OF SCIENCE IN AVIATION SAFETY",
                        "MASTER OF SCIENCE IN AVIATION SECURITY",
                        "MASTER OF SCIENCE IN ENGINEERING BUSINESS MANAGEMENT",
                        "MASTER OF SCIENCE IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN AVIATION SAFETY",
                        "POSTGRADUATE DIPLOMA IN AVIATION SECURITY",
                        "POSTGRADUATE DIPLOMA IN BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES COLLEGE FOR ADVANCED EDUCATION",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF EDUCATION"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN EDUCATION- EDUCATIONAL NEUROSCIENCE",
                        "DOCTOR OF PHILOSOPHY IN EDUCATION: MEASUREMENT AND ASSESSMENT",
                        "DOCTOR OF PHILOSOPHY IN EDUCATION: SPECIAL EDUCATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF EDUCATION",
                        "MASTER OF EDUCATION IN APPLIED BEHAVIOR ANALYSIS",
                        "MASTER OF EDUCATION IN CURRICULUM AND LEARNING DESIGN",
                        "MASTER OF EDUCATION IN EDUCATIONAL ASSESSMENT",
                        "MASTER OF EDUCATION IN SPECIAL AND INCLUSIVE EDUCATION"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN APPLIED BEHAVIOR ANALYSIS",
                        "POSTGRADUATE DIPLOMA IN EDUCATION",
                        "POSTGRADUATE DIPLOMA IN GUIDANCE AND COUNSELLING",
                        "POSTGRADUATE DIPLOMA IN SCHOOL EVALUATION AND IMPROVEMENT"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES COLLEGE OF TECHNOLOGY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF APPLIED HEALTH SCIENCES",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF FINANCIAL SCIENCES",
                        "BACHELOR OF MASS COMMUNICATION IN JOURNALISM",
                        "BACHELOR OF MASS COMMUNICATION IN PUBLIC RELATIONS AND ADVERTISING",
                        "BACHELOR OF MASS COMMUNICATION IN RADIO AND TELEVISION",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN HEALTHCARE MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MEDICAL DIAGNOSTIC IMAGING",
                        "BACHELOR OF SCIENCE IN MEDICAL LABORATORY SCIENCES"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN ACCOUNTING",
                        "DIPLOMA IN BANKING AND FINANCE",
                        "DIPLOMA IN BUSINESS ADMINISTRATION AND COMPUTER INFORMATION SYSTEMS (DOUBLE MAJOR)",
                        "DIPLOMA IN COMPUTER GRAPHIC DESIGN AND ANIMATION",
                        "DIPLOMA IN HUMAN RESOURCE MANAGEMENT"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES DIPLOMATIC ACADEMY",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF ARTS IN DIPLOMACY AND INTERNATIONAL RELATIONS"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN UAE DIPLOMACY AND INTERNATIONAL RELATIONS"
                    ]
                }
            ]
        },
        {
            "institute": "EMIRATES INSTITUTE FOR BANKING AND FINANCIAL STUDIES",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF SCIENCE IN BANKING AND FINANCE"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN BANKING",
                        "DIPLOMA IN ISLAMIC BANKING"
                    ]
                },
                {
                    "level": "HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN BANKING"
                    ]
                }
            ]
        },
        {
            "institute": "EUROPEAN UNIVERSITY COLLEGE",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN RESTORATIVE AND PROSTHODONTICS",
                        "MASTER OF SCIENCE IN ORAL IMPLANTOLOGY",
                        "MASTER OF SCIENCE IN ORTHODONTICS"
                    ]
                },
                {
                    "level": "GRADUATE CERTIFICATE",
                    "fields": [
                        "SPECIALTY CERTIFICATE IN ENDODONTICS",
                        "SPECIALTY CERTIFICATE IN ORTHODONTICS",
                        "SPECIALTY CERTIFICATE IN PEDIATRIC DENTISTRY"
                    ]
                }
            ]
        },
        {
            "institute": "FATIMA COLLEGE OF HEALTH SCIENCES",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF EMERGENCY HEALTH (PARAMEDICS)",
                        "BACHELOR OF MEDICAL IMAGING",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF PHYSIOTHERAPY",
                        "BACHELOR OF SCIENCE IN NURSING",
                        "BACHELOR OF SCIENCE IN PSYCHOLOGY"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN BIOMEDICAL EQUIPMENT SERVICES",
                        "DIPLOMA IN EMERGENCY HEALTH (PARAMEDICS)",
                        "DIPLOMA IN MEDICAL ASSISTANT",
                        "DIPLOMA IN MEDICAL LABORATORY ANALYSIS"
                    ]
                },
                {
                    "level": "UNDERGRADUATE HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN EMERGENCY HEALTH (PARAMEDICS)",
                        "HIGHER DIPLOMA IN PHARMACY",
                        "HIGHER DIPLOMA IN PHYSIOTHERAPY",
                        "HIGHER DIPLOMA IN RADIOGRAPHY"
                    ]
                }
            ]
        },
        {
            "institute": "GULF MEDICAL UNIVERSITY",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE DEGREE IN PRECLINICAL SCIENCES"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BIOMEDICAL SCIENCES",
                        "BACHELOR OF DENTISTRY",
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY",
                        "BACHELOR OF PHYSIOTHERAPY",
                        "BACHELOR OF SCIENCE IN HEALTHCARE MANAGEMENT AND ECONOMICS",
                        "BACHELOR OF SCIENCE IN NURSING",
                        "BACHELOR OF SCIENCE- ANESTHESIA TECHNOLOGY",
                        "BACHELOR OF SCIENCE- MEDICAL IMAGING SCIENCES",
                        "BACHELOR OF SCIENCE- MEDICAL LABORATORY SCIENCES",
                        "DOCTOR OF PHARMACY"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN ENVIRONMENTAL HEALTH AND TOXICOLOGY"
                    ]
                },
                {
                    "level": "HIGHER DIPLOMA",
                    "fields": [
                        "JOINT DIPLOMA IN HEALTH PROFESSIONS EDUCATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER IN HEALTHCARE MANAGEMENT AND ECONOMICS",
                        "JOINT MASTERS IN HEALTH PROFESSIONS EDUCATION",
                        "MASTER IN CLINICAL PHARMACY",
                        "MASTER IN ENDODONTOLOGY",
                        "MASTER IN ENVIRONMENTAL HEALTH AND TOXICOLOGY",
                        "MASTER IN PUBLIC HEALTH",
                        "MASTER OF DENTAL SURGERY IN PERIODONTOLOGY",
                        "MASTER OF PHYSICAL THERAPY",
                        "MASTER OF SCIENCE IN DRUG DISCOVERY AND DEVELOPMENT",
                        "MASTER OF SCIENCE IN MEDICAL LABORATORY SCIENCES",
                        "MASTERS IN HUMAN REPRODUCTIVE BIOLOGY"
                    ]
                }
            ]
        },
        {
            "institute": "HAMDAN BIN MOHAMMED SMART UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS AND HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF BUSINESS AND QUALITY MANAGEMENT",
                        "BACHELOR OF BUSINESS IN ACCOUNTING",
                        "BACHELOR OF BUSINESS IN MARKETING",
                        "BACHELOR OF SCIENCE IN HEALTH ADMINISTRATION"
                    ]
                },
                {
                    "level": "DIPLOMA",
                    "fields": [
                        "DIPLOMA IN HEALTH ADMINISTRATION",
                        "DIPLOMA OF BUSINESS AND QUALITY MANAGEMENT"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION IN ISLAMIC BANKING AND FINANCE",
                        "MASTER IN ISLAMIC BANKING AND FINANCE",
                        "MASTER OF ARTS IN ONLINE CURRICULUM AND INSTRUCTION",
                        "MASTER OF ARTS IN ONLINE EDUCATION LEADERSHIP AND MANAGEMENT",
                        "MASTER OF EDUCATION IN GIFTED AND TALENTED EDUCATION",
                        "MASTER OF HUMAN RESOURCES MANAGEMENT",
                        "MASTER OF MANAGEMENT IN ENTREPRENEURIAL LEADERSHIP",
                        "MASTER OF PROJECT MANAGEMENT",
                        "MASTER OF SCIENCE : EXCELLENCE IN ENVIRONMENTAL MANAGEMENT",
                        "MASTER OF SCIENCE IN HOSPITAL MANAGEMENT",
                        "MASTER OF SCIENCE IN INNOVATION AND CHANGE MANAGEMENT",
                        "MASTER OF SCIENCE IN INTERACTIVE EDUCATIONAL TECHNOLOGIES",
                        "MASTER OF SCIENCE IN ORGANIZATIONAL EXCELLENCE",
                        "MASTER OF SCIENCE IN PUBLIC HEALTH"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN EDUCATION IN GIFTED AND TALENTED EDUCATION"
                    ]
                }
            ]
        },
        {
            "institute": "HIGHER COLLEGES OF TECHNOLOGY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF MARKETING",
                        "BACHELOR OF ACCOUNTING",
                        "BACHELOR OF AERONAUTICAL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF APPLIED MEDIA",
                        "BACHELOR OF APPLIED SCIENCE IN MARINE ENGINEERING TECHNOLOGY",
                        "BACHELOR OF APPLIED SCIENCE IN MARINE TRANSPORT",
                        "BACHELOR OF APPLIED SCIENCE IN MARITIME ENGINEERING TECHNOLOGY AND NAVAL ARCHITECTURE",
                        "BACHELOR OF AVIATION MAINTENANCE ENGINEERING TECHNOLOGY - AIRFRAME AND AERO ENGINES",
                        "BACHELOR OF AVIATION MAINTENANCE ENGINEERING TECHNOLOGY - AVIONICS",
                        "BACHELOR OF BUSINESS ANALYTICS",
                        "BACHELOR OF CHEMICAL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF CIVIL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF EDUCATION EARLY CHILDHOOD",
                        "BACHELOR OF ELECTRICAL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF EMERGENCY MEDICAL SERVICES",
                        "BACHELOR OF FINANCE",
                        "BACHELOR OF HEALTH INFORMATION MANAGEMENT",
                        "BACHELOR OF HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF INDUSTRIAL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF INFORMATION SYSTEMS",
                        "BACHELOR OF INFORMATION TECHNOLOGY",
                        "BACHELOR OF INNOVATION AND ENTREPRENEURSHIP MANAGEMENT",
                        "BACHELOR OF LOGISTICS AND SUPPLY CHAIN MANAGEMENT",
                        "BACHELOR OF LOGISTICS ENGINEERING TECHNOLOGY",
                        "BACHELOR OF MECHANICAL ENGINEERING TECHNOLOGY",
                        "BACHELOR OF MECHATRONICS ENGINEERING TECHNOLOGY",
                        "BACHELOR OF MEDICAL IMAGING SCIENCE",
                        "BACHELOR OF MEDICAL LAB SCIENCE",
                        "BACHELOR OF NAVAL SCIENCE",
                        "BACHELOR OF NURSING",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF QUALITY MANAGEMENT",
                        "BACHELOR OF SCIENCE IN AVIATION SCIENCE",
                        "BACHELOR OF SCIENCE IN AVIATION SUPPORT",
                        "BACHELOR OF SOCIAL WORK",
                        "BACHELOR OF TOURISM MANAGEMENT",
                        "BACHELOR OF VETERINARY SCIENCE"
                    ]
                },
                {
                    "level": "DIPLOMA",
                    "fields": [
                        "DIPLOMA OF AERONAUTICAL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF AUDITING AND DISCIPLINE POLICING",
                        "DIPLOMA OF AVIATION MAINTENANCE ENGINEERING TECHNOLOGY - AIRFRAME AND AERO ENGINES",
                        "DIPLOMA OF AVIATION MAINTENANCE ENGINEERING TECHNOLOGY - AVIONICS",
                        "DIPLOMA OF BORDER SECURITY OPERATIONS",
                        "DIPLOMA OF CHEMICAL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF CHILD PROTECTION",
                        "DIPLOMA OF CIVIL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF CRIMINAL JUSTICE",
                        "DIPLOMA OF ELECTRICAL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF EMERGENCY MEDICAL SERVICES",
                        "DIPLOMA OF INDUSTRIAL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF LAW ENFORCEMENT",
                        "DIPLOMA OF LOGISTICS ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF MECHANICAL ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF MECHATRONICS ENGINEERING TECHNOLOGY",
                        "DIPLOMA OF MEDICAL LAB TECHNOLOGY",
                        "DIPLOMA OF NAVAL SCIENCE",
                        "DIPLOMA OF PHARMACY"
                    ]
                },
                {
                    "level": "HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA OF ACCOUNTING",
                        "HIGHER DIPLOMA OF APPLIED MEDIA",
                        "HIGHER DIPLOMA OF BUSINESS ANALYTICS",
                        "HIGHER DIPLOMA OF FINANCE",
                        "HIGHER DIPLOMA OF HEALTH INFORMATION CODING",
                        "HIGHER DIPLOMA OF HUMAN RESOURCE MANAGEMENT",
                        "HIGHER DIPLOMA OF INFORMATION SYSTEMS",
                        "HIGHER DIPLOMA OF INFORMATION TECHNOLOGY",
                        "HIGHER DIPLOMA OF INNOVATION AND ENTREPRENEURSHIP MANAGEMENT",
                        "HIGHER DIPLOMA OF LOGISTICS AND SUPPLY CHAIN MANAGEMENT",
                        "HIGHER DIPLOMA OF MARKETING",
                        "HIGHER DIPLOMA OF MEDICAL IMAGING TECHNOLOGY",
                        "HIGHER DIPLOMA OF QUALITY MANAGEMENT",
                        "HIGHER DIPLOMA OF TOURISM MANAGEMENT",
                        "HIGHER DIPLOMA OF VETERINARY LABORATORY TECHNOLOGY"
                    ]
                }
            ]
        },
        {
            "institute": "IMAM MALIK COLLEGE FOR ISLAMIC SHARIA AND LAW",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN LAW",
                        "BACHELOR OF SHARIA'"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN FIQH AND ITS FUNDAMENTALS",
                        "MASTER IN PRIVATE LAW",
                        "MASTER IN PUBLIC LAW"
                    ]
                }
            ]
        },
        {
            "institute": "INSEAD- THE BUSINESS SCHOOL FOR THE WORLD, ABU DHABI",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "INSTITUTE OF MANAGEMENT TECHNOLOGY-DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF COMMERCE IN ACCOUNTANCY",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "JUMEIRA UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF EDUCATION IN EARLY \u200ECHILDHOOD EDUCATION \u200E",
                        "BACHELOR OF ISLAMIC STUDIES",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL \u200EHEALTH",
                        "BACHELOR OF SCIENCE IN HEALTHCARE MANAGEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN FIQH AND ITS FUNDAMENTALS"
                    ]
                }
            ]
        },
        {
            "institute": "KHALIFA UNIVERSITY (FORMERLY: KHALIFA UNIVERSITY OF SCIENCE AND TECHNOLOGY)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF SCIENCE IN AEROSPACE ENGINEERING",
                        "BACHELOR OF SCIENCE IN APPLIED MATHEMATICS AND STATISTICS",
                        "BACHELOR OF SCIENCE IN BIOMEDICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL AND SYSTEMS ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN PETROLEUM ENGINEERING",
                        "BACHELOR OF SCIENCE IN PETROLEUM GEOSCIENCES",
                        "BACHELOR OF SCIENCE IN PHYSICS",
                        "DOCTOR OF MEDICINE"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN CHEMICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN ELECTRICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN INTERDISCIPLINARY ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN MECHANICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN PETROLEUM ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN PETROLEUM GEOSCIENCE"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASER OF ARTS IN INTERNATIONAL AND CIVIL SECURITY",
                        "MASTER OF ENGINEERING IN CHEMICAL ENGINEERING",
                        "MASTER OF ENGINEERING IN ELECTRICAL ENGINEERING",
                        "MASTER OF ENGINEERING IN HEALTH, SAFETY AND ENVIRONMENTAL ENGINEERING",
                        "MASTER OF ENGINEERING IN MECHANICAL ENGINEERING",
                        "MASTER OF ENGINEERING IN PETROLEUM ENGINEERING",
                        "MASTER OF SCIENCE BY RESEARCH IN ENGINEERING",
                        "MASTER OF SCIENCE IN APPLIED CHEMISTRY",
                        "MASTER OF SCIENCE IN BIOMEDICAL ENGINEERING",
                        "MASTER OF SCIENCE IN CHEMICAL ENGINEERING",
                        "MASTER OF SCIENCE IN CHEMICAL \u200EENGINEERING PRACTICE",
                        "MASTER OF SCIENCE IN CIVIL AND INFRASTRUCTURAL ENGINEERING",
                        "MASTER OF SCIENCE IN COMPUTER SCIENCE",
                        "MASTER OF SCIENCE IN CYBER SECURITY",
                        "MASTER OF SCIENCE IN ELECTRICAL AND COMPUTER ENGINEERING",
                        "MASTER OF SCIENCE IN ENGINEERING SYSTEMS AND MANAGEMENT",
                        "MASTER OF SCIENCE IN MATERIALS SCIENCE AND ENGINEERING",
                        "MASTER OF SCIENCE IN MECHANICAL ENGINEERING",
                        "MASTER OF SCIENCE IN MECHANICAL \u200EENGINEERING PRACTICE",
                        "MASTER OF SCIENCE IN NUCLEAR ENGINEERING",
                        "MASTER OF SCIENCE IN PETROLEUM GEOSCIENCE",
                        "MASTER OF SCIENCE IN SUSTAINABLE CRITICAL INFRASTRUCTURE",
                        "MASTER OF SCIENCE IN WATER AND ENVIRONMENTAL ENGINEERING"
                    ]
                }
            ]
        },
        {
            "institute": "MOHAMED BIN ZAYED UNIVERSITY OF ARTIFICIAL INTELLIGENCE",
            "levels": [
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN COMPUTER VISION",
                        "DOCTOR OF PHILOSOPHY IN MACHINE LEARNING",
                        "DOCTOR OF PHILOSOPHY IN NATURAL LANGUAGE PROCESSING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN COMPUTER VISION",
                        "MASTER OF SCIENCE IN MACHINE LEARNING",
                        "MASTER OF SCIENCE IN NATURAL LANGUAGE PROCESSING"
                    ]
                }
            ]
        },
        {
            "institute": "MOHAMMED BIN RASHID SCHOOL OF GOVERNMENT",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF PUBLIC ADMINISTRATION (ARABIC)",
                        "EXECUTIVE MASTER OF PUBLIC ADMINISTRATION (ENGLISH)",
                        "MASTER IN INNOVATION MANAGEMENT",
                        "MASTER IN PUBLIC POLICY",
                        "MASTER OF PUBLIC ADMINISTRATION"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN INNOVATION MANAGEMENT",
                        "POSTGRADUATE DIPLOMA IN PUBLIC ADMINISTRATION (ARABIC)",
                        "POSTGRADUATE DIPLOMA IN PUBLIC ADMINISTRATION (ENGLISH)",
                        "POSTGRADUATE DIPLOMA IN PUBLIC POLICY"
                    ]
                }
            ]
        },
        {
            "institute": "MOHAMMED BIN RASHID UNIVERSITY OF MEDICINE AND HEALTH SCIENCES",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN BIOMEDICAL SCIENCES",
                        "MASTER OF SCIENCE IN CARDIOVASCULAR NURSING",
                        "MASTER OF SCIENCE IN ENDODONTICS",
                        "MASTER OF SCIENCE IN ORTHODONTICS",
                        "MASTER OF SCIENCE IN PEDIATRIC DENTISTRY",
                        "MASTER OF SCIENCE IN PEDIATRIC NURSING",
                        "MASTER OF SCIENCE IN PERIODONTOLOGY",
                        "MASTER OF SCIENCE IN PROSTHODONTICS"
                    ]
                }
            ]
        },
        {
            "institute": "MOHAMMED V UNIVERSITY- ABU DHABI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR OF ISLAMIC STUDIES"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN ISLAMIC FIQH AND ISSUES OF THE CONTEMPORARY SOCIETY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN MALIKI DENOMINATION AND CONTEMPORARY ISSUES"
                    ]
                }
            ]
        },
        {
            "institute": "NATIONAL DEFENSE COLLEGE",
            "levels": [
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN STRATEGIC STUDIES"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF STRATEGIC AND SECURITY STUDIES"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN STRATEGIC AND SECURITY STUDIES"
                    ]
                }
            ]
        },
        {
            "institute": "NEW YORK INSTITUTE OF TECHNOLOGY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF FINE ARTS IN COMPUTER GRAPHICS",
                        "BACHELOR OF FINE ARTS IN INTERIOR DESIGN",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF SCIENCE IN INFORMATION, NETWORK AND COMPUTER SECURITY",
                        "MASTER OF SCIENCE IN INSTRUCTIONAL TECHNOLOGY"
                    ]
                }
            ]
        },
        {
            "institute": "NEW YORK UNIVERSITY, ABU DHABI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN ARAB CROSSROADS STUDIES",
                        "BACHELOR OF ARTS IN ART AND ART HISTORY",
                        "BACHELOR OF ARTS IN ECONOMICS",
                        "BACHELOR OF ARTS IN FILM AND NEW MEDIA",
                        "BACHELOR OF ARTS IN HISTORY",
                        "BACHELOR OF ARTS IN INTERACTIVE MEDIA",
                        "BACHELOR OF ARTS IN LEGAL STUDIES",
                        "BACHELOR OF ARTS IN LITERATURE AND CREATIVE WRITING",
                        "BACHELOR OF ARTS IN MUSIC",
                        "BACHELOR OF ARTS IN PHILOSOPHY",
                        "BACHELOR OF ARTS IN POLITICAL SCIENCE",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF ARTS IN SOCIAL RESEARCH AND PUBLIC POLICY",
                        "BACHELOR OF ARTS IN THEATER",
                        "BACHELOR OF SCIENCE IN BIOENGINEERING",
                        "BACHELOR OF SCIENCE IN BIOLOGY",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN GENERAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MATHEMATICS",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN PHYSICS"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF FINE ARTS IN ART AND MEDIA",
                        "MASTER OF SCIENCE IN ECONOMICS"
                    ]
                }
            ]
        },
        {
            "institute": "POLICE COLLEGE, ABU DHABI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN APPLIED POLICE SCIENCES"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN CRIMINAL JUSTICE",
                        "DIPLOMA IN POLICE ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "POLICE SCIENCES ACADEMY- SHARJAH",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF POLICE SCIENCES"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTORATE OF POLICE SCIENCES IN CRIMINAL INVESTIGATION",
                        "DOCTORATE OF POLICE SCIENCES IN POLICE ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN POLICE SCIENCES- QUALITY AND EXCELLENCE IN SECURITY OPERATIONS",
                        "MASTER OF POLICE SCIENCE IN CRIMINAL INVESTIGATION",
                        "MASTER OF POLICE SCIENCE IN POLICE ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "RABDAN ACADEMY",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE DEGREE IN BUSINESS CONTINUITY MANAGEMENT",
                        "ASSOCIATE DEGREE IN INTEGRATED EMERGENCY MANAGEMENT",
                        "ASSOCIATE DEGREE IN POLICING AND SECURITY",
                        "ASSOCIATE IN HOMELAND SECURITY"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF SCIENCE IN BUSINESS CONTINUITY MANAGEMENT",
                        "BACHELOR OF SCIENCE IN COMPREHENSIVE POLICE STATION MANAGEMENT",
                        "BACHELOR OF SCIENCE IN HOMELAND SECURITY",
                        "BACHELOR OF SCIENCE IN INTEGRATED EMERGENCY MANAGEMENT",
                        "BACHELOR OF SCIENCE IN POLICING AND SECURITY"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN COMPREHENSIVE POLICE STATION MANAGEMENT",
                        "DIPLOMA IN CRIME SCENE",
                        "HIGHER DIPLOMA IN BUSINESS CONTINUITY MANAGEMENT",
                        "HIGHER DIPLOMA IN COMPREHENSIVE POLICE STATION MANAGEMENT",
                        "HIGHER DIPLOMA IN INTEGRATED EMERGENCY MANAGEMENT",
                        "HIGHER DIPLOMA IN POLICING AND SECURITY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN INTELLIGENCE ANALYSIS",
                        "MASTER OF SCIENCE IN POLICING AND SECURITY LEADERSHIP"
                    ]
                }
            ]
        },
        {
            "institute": "RAS AL KHAIMAH MEDICAL AND HEALTH SCIENCES UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF DENTAL SURGERY",
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF SCIENCE IN NURSING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN CLINICAL PHARMACY",
                        "MASTER OF SCIENCE IN MIDWIFERY",
                        "MASTER OF SCIENCE IN NURSING",
                        "MASTER OF SCIENCE IN PHARMACEUTICAL CHEMISTRY",
                        "MASTER OF SCIENCE IN PHARMACEUTICS"
                    ]
                }
            ]
        },
        {
            "institute": "ROCHESTER INSTITUTE OF TECHNOLOGY- DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - FINANCE",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - MANAGEMENT",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION - MARKETING",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINISTRATION- INTERNATIONAL BUSINESS",
                        "BACHELOR OF SCIENCE IN COMPUTING AND INFORMATION TECHNOLOGIES",
                        "BACHELOR OF SCIENCE IN COMPUTING SECURITY",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF ENGINEERING IN ENGINEERING MANAGEMENT",
                        "MASTER OF ENGINEERING IN MECHANICAL ENGINEERING",
                        "MASTER OF SCIENCE IN COMPUTING SECURITY",
                        "MASTER OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "MASTER OF SCIENCE IN FINANCE",
                        "MASTER OF SCIENCE IN NETWORKING AND SYSTEM ADMINISTRATION",
                        "MASTER OF SCIENCE IN PROFESSIONAL STUDIES- CITY SCIENCES",
                        "MASTER OF SCIENCE IN PROFESSIONAL STUDIES- DATA ANALYTICS",
                        "MASTER OF SCIENCE IN SERVICE LEADERSHIP AND INNOVATION"
                    ]
                }
            ]
        },
        {
            "institute": "ROYAL COLLEGE OF SURGEONS IN IRELAND- DUBAI",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF SCIENCE IN HEALTHCARE MANAGEMENT",
                        "MASTER OF SCIENCE IN LEADERSHIP AND INNOVATION IN HEALTHCARE",
                        "MASTER OF SCIENCE IN QUALITY AND SAFETY IN HEALTHCARE MANAGEMENT"
                    ]
                }
            ]
        },
        {
            "institute": "SAINT JOSEPH UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF LAW"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "LLM IN BUSINESS LAW",
                        "MASTER IN TRANSLATION"
                    ]
                }
            ]
        },
        {
            "institute": "SKYLINE UNIVERSITY COLLEGE",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION IN ACCOUNTING AND FINANCE",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN HUMAN RESOURCE MANAGEMENT AND PSYCHOLOGY",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN INFORMATION SYSTEMS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN INNOVATION AND ENTREPRENEURSHIP",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN INTERNATIONAL BUSINESS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MARKETING AND RETAIL MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN PUBLIC ADMINISTRATION",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN TOURISM AND HOSPITALITY MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "SORBONNE UNIVERSITY, ABU DHABI (PREVIOUSLY, PARIS SORBONNE UNIVERSITY, ABU DHABI)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN APPLIED FOREIGN LANGUAGES",
                        "BACHELOR IN ECONOMICS AND MANAGEMENT",
                        "BACHELOR IN FRENCH LITERATURE",
                        "BACHELOR IN GEOGRAPHY AND PLANNING",
                        "BACHELOR IN HISTORY",
                        "BACHELOR IN HISTORY OF ART AND ARCHAEOLOGY",
                        "BACHELOR IN LAW",
                        "BACHELOR IN PHILOSOPHY AND SOCIOLOGY",
                        "BACHELOR IN PHYSICS",
                        "BACHELOR IN RECORDS MANAGEMENT AND ARCHIVAL SCIENCE"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN SCIENCE AND MANAGEMENT"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER IN APPLIED FOREIGN LANGUAGES",
                        "MASTER IN APPLIED SOCIOLOGICAL RESEARCH",
                        "MASTER IN ARTS IN MUSLIM AND ARAB WORLD STUDIES",
                        "MASTER IN BANKING AND FINANCE",
                        "MASTER IN ENVIRONMENT: DYNAMICS OF TERRITORIES AND SOCIETIES",
                        "MASTER IN HEALTH ECONOMICS",
                        "MASTER IN HISTORY OF ART AND MUSEUM STUDIES",
                        "MASTER IN INTERNATIONAL BUSINESS LAW",
                        "MASTER IN INTERNATIONAL LAW, INTERNATIONAL RELATIONS AND DIPLOMACY",
                        "MASTER IN MANAGEMENT OF PERFORMING ARTS AND EVENTS",
                        "MASTER IN MARKETING, ADVERTISING AND COMMUNICATION",
                        "MASTER IN PUBLISHING, INFORMATION AND MULTIMEDIA",
                        "MASTER IN TEACHING FRENCH AS A FOREIGN LANGUAGE",
                        "MASTER IN URBAN PLANNING AND DEVELOPMENT"
                    ]
                }
            ]
        },
        {
            "institute": "UMM AL QUWAIN UNIVERSITY (FORMERLY: EMIRATES CANADIAN UNIVERSITY COLLEGE)",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF ENGLISH LANGUAGE AND TRANSLATION",
                        "BACHELOR OF LAW"
                    ]
                }
            ]
        },
        {
            "institute": "UNITED ARAB EMIRATES UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ACCOUNTING",
                        "BACHELOR OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN ENGLISH LITERATURE",
                        "BACHELOR OF ARTS IN GEOGRAPHY",
                        "BACHELOR OF ARTS IN HISTORY",
                        "BACHELOR OF ARTS IN LINGUISTICS",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN POLITICAL SCIENCE",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF ARTS IN SOCIOLOGY",
                        "BACHELOR OF ARTS IN TOURISM STUDIES",
                        "BACHELOR OF ARTS IN TRANSLATION STUDIES",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF ECONOMICS",
                        "BACHELOR OF EDUCATION IN EARLY CHILDHOOD",
                        "BACHELOR OF EDUCATION IN HEALTH AND PHYSICAL EDUCATION",
                        "BACHELOR OF EDUCATION IN SPECIAL EDUCATION",
                        "BACHELOR OF FINANCE AND BANKING",
                        "BACHELOR OF LAW",
                        "BACHELOR OF SCIENCE IN AEROSPACE ENGINEERING",
                        "BACHELOR OF SCIENCE IN AGRICULTURAL RESOURCE MANAGEMENT",
                        "BACHELOR OF SCIENCE IN ARCHITECTURAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN BIOCHEMISTRY",
                        "BACHELOR OF SCIENCE IN BIOLOGY",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMMUNICATION ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN DIETETICS",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN FOOD SCIENCE",
                        "BACHELOR OF SCIENCE IN GEOSCIENCES",
                        "BACHELOR OF SCIENCE IN HORTICULTURE",
                        "BACHELOR OF SCIENCE IN INFORMATION SECURITY",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN MARINE FISHERIES AND ANIMAL SCIENCE",
                        "BACHELOR OF SCIENCE IN MATHEMATICS",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN NUTRITIONAL SCIENCE",
                        "BACHELOR OF SCIENCE IN PETROLEUM ENGINEERING",
                        "BACHELOR OF SCIENCE IN PHYSICS",
                        "BACHELOR OF SCIENCE IN SPEECH LANGUAGE PATHOLOGY",
                        "BACHELOR OF SOCIAL WORK",
                        "BACHELOR OF STATISTICS"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF MEDICINE",
                        "DOCTOR OF PHARMACY",
                        "DOCTOR OF PHILOSOPHY IN ARCHITECTURAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN BIOMEDICAL SCIENCES",
                        "DOCTOR OF PHILOSOPHY IN CELLULAR AND MOLECULAR BIOLOGY",
                        "DOCTOR OF PHILOSOPHY IN CHEMICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN CHEMISTRY",
                        "DOCTOR OF PHILOSOPHY IN CIVIL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN ECOLOGY AND ENVIRONMENTAL SCIENCES",
                        "DOCTOR OF PHILOSOPHY IN ELECTRICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN GEOSCIENCES",
                        "DOCTOR OF PHILOSOPHY IN INFORMATICS AND COMPUTING",
                        "DOCTOR OF PHILOSOPHY IN LANGUAGE AND LITERACY EDUCATION",
                        "DOCTOR OF PHILOSOPHY IN LAW",
                        "DOCTOR OF PHILOSOPHY IN LEADERSHIP AND POLICY STUDIES IN EDUCATION",
                        "DOCTOR OF PHILOSOPHY IN MATHEMATICS",
                        "DOCTOR OF PHILOSOPHY IN MATHEMATICS EDUCATION",
                        "DOCTOR OF PHILOSOPHY IN MECHANICAL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN PHYSICS",
                        "DOCTOR OF PHILOSOPHY IN PUBLIC HEALTH",
                        "DOCTOR OF PHILOSOPHY IN SCIENCE EDUCATION",
                        "DOCTOR OF PHILOSOPHY IN SPECIAL EDUCATION",
                        "DOCTORATE OF BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF EDUCATION",
                        "MASTER OF EDUCATIONAL INNOVATION",
                        "MASTER OF ENGINEERING MANAGEMENT",
                        "MASTER OF GOVERNANCE AND PUBLIC POLICY",
                        "MASTER OF MEDICAL SCIENCES",
                        "MASTER OF PRIVATE LAW",
                        "MASTER OF PROFESSIONAL ACCOUNTING",
                        "MASTER OF PUBLIC HEALTH",
                        "MASTER OF PUBLIC LAW",
                        "MASTER OF SCIENCE IN ARCHITECTURAL ENGINEERING",
                        "MASTER OF SCIENCE IN CHEMICAL ENGINEERING",
                        "MASTER OF SCIENCE IN CHEMISTRY",
                        "MASTER OF SCIENCE IN CIVIL ENGINEERING",
                        "MASTER OF SCIENCE IN CLINICAL PSYCHOLOGY",
                        "MASTER OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "MASTER OF SCIENCE IN ENVIRONMENTAL SCIENCES",
                        "MASTER OF SCIENCE IN FOOD SCIENCE",
                        "MASTER OF SCIENCE IN HORTICULTURE",
                        "MASTER OF SCIENCE IN HUMAN NUTRITION",
                        "MASTER OF SCIENCE IN INFORMATION SECURITY",
                        "MASTER OF SCIENCE IN IT MANAGEMENT",
                        "MASTER OF SCIENCE IN MATHEMATICS",
                        "MASTER OF SCIENCE IN MECHANICAL ENGINEERING",
                        "MASTER OF SCIENCE IN MOLECULAR BIOLOGY AND BIOTECHNOLOGY",
                        "MASTER OF SCIENCE IN PETROLEUM ENGINEERING",
                        "MASTER OF SCIENCE IN PHYSICS",
                        "MASTER OF SCIENCE IN REMOTE SENSING AND GEOGRAPHIC INFORMATION SYSTEMS",
                        "MASTER OF SCIENCE IN SOFTWARE ENGINEERING",
                        "MASTER OF SCIENCE IN SPACE SCIENCE",
                        "MASTER OF SCIENCE IN WATER RESOURCES",
                        "MASTER OF SOCIAL WORK"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY COLLEGE OF MOTHER AND FAMILY SCIENCES",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN FAMILY SCIENCES \u2013MAJOR HUMAN RIGHTS",
                        "BACHELOR OF ARTS IN FAMILY SCIENCES \u2013MAJOR SOCIAL COUNSELLING",
                        "BACHELOR OF ARTS IN FAMILY SCIENCES \u2013MAJOR SOCIAL SERVICES MANAGEMENT"
                    ]
                },
                {
                    "level": "UNDERGRADUATE DIPLOMA",
                    "fields": [
                        "DIPLOMA IN FAMILY SCIENCES"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF BALAMAND IN DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF SCIENCE IN BIOLOGY",
                        "BACHELOR OF SCIENCE IN CHEMICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MATHEMATICS",
                        "BACHELOR OF SCIENCE IN PHYSICS"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL TEACHING DIPLOMA"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF BIRMINGHAM DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ENGINEERING IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN ACCOUNTING AND FINANCE",
                        "BACHELOR OF SCIENCE IN ARTIFICIAL INTELLIGENCE AND COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN BUSINESS MANAGEMENT AND MARKETING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN ECONOMICS",
                        "BACHELOR OF SCIENCE IN MONEY, BANKING AND FINANCE",
                        "BACHELOR OF SCIENCE IN PSYCHOLOGY"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "LLM International Commercial Law",
                        "MASTER OF SCIENCE IN COMPUTER SCIENCE",
                        "Master of Science in International Business"
                    ]
                },
                {
                    "level": "POSTGRADUATE CERTIFICATE",
                    "fields": [
                        "POSTGRADUATE CERTIFICATE IN EDUCATION IN PRIMARY EDUCATION",
                        "POSTGRADUATE CERTIFICATE IN EDUCATION IN SECONDARY EDUCATION ENGLISH",
                        "POSTGRADUATE CERTIFICATE IN EDUCATION IN SECONDARY EDUCATION MATHEMATICS",
                        "POSTGRADUATE CERTIFICATE IN EDUCATION IN SECONDARY EDUCATION MODERN LANGUAGES: ARABIC",
                        "POSTGRADUATE CERTIFICATE IN EDUCATION IN SECONDARY EDUCATION SCIENCES",
                        "POSTGRADUATE CERTIFICATE IN INCLUSION AND SPECIAL EDUCATION NEEDS"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION IN ACCOUNTING",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN BUSINESS ECONOMICS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN CUSTOMS",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN ENTREPRENEURSHIP MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN FINANCE AND BANKING",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MANAGEMENT",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN MARKETING",
                        "BACHELOR OF BUSINESS ADMINISTRATION IN SUPPLY CHAIN AND LOGISTICS MANAGEMENT",
                        "BACHELOR OF SCIENCE IN COMPUTING AND INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF PHILOSOPHY IN BUSINESS ADMINISTRATION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF LAWS",
                        "MASTER OF SCIENCE IN DATA SCIENCE"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF FUJAIRAH",
            "levels": [
                {
                    "level": "ASSOCIATE",
                    "fields": [
                        "ASSOCIATE OF ARTS IN BUSINESS ADMINISTRATION",
                        "ASSOCIATE OF ARTS IN INFORMATION TECHNOLOGY"
                    ]
                },
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN ARABIC LANGUAGE & LITERATURE",
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF INFORMATION TECHNOLOGY",
                        "BACHELOR OF MASS COMMUNICATION IN PUBLIC RELATION"
                    ]
                },
                {
                    "level": "DIPLOMA",
                    "fields": [
                        "DIPLOMA IN PUBLIC RELATIONS"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF SCIENCE AND TECHNOLOGY OF FUJAIRAH",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN PSYCHOLOGY",
                        "BACHELOR OF ARTS IN SOCIOLOGY AND SOCIAL WORK",
                        "BACHELOR OF INTERIOR DESIGN",
                        "BACHELOR OF LAW",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF SCIENCE IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN MANAGEMENT",
                        "DOCTOR OF DENTAL SURGERY"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF SHARJAH",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR IN LAW",
                        "BACHELOR OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN ART HISTORY AND MUSEUM STUDIES",
                        "BACHELOR OF ARTS IN COMMUNICATION",
                        "BACHELOR OF ARTS IN ENGLISH LANGUAGE AND LITERATURE",
                        "BACHELOR OF ARTS IN FASHION DESIGN WITH TEXTILES",
                        "BACHELOR OF ARTS IN FINE ARTS",
                        "BACHELOR OF ARTS IN GRAPHIC DESIGN AND MULTIMEDIA",
                        "BACHELOR OF ARTS IN HISTORY AND ISLAMIC CIVILIZATION",
                        "BACHELOR OF ARTS IN HISTORY AND ISLAMIC CIVILIZATION- TOURIST GUIDE",
                        "BACHELOR OF ARTS IN INTERIOR ARCHITECTURE AND DESIGN",
                        "BACHELOR OF ARTS IN INTERNATIONAL RELATIONS",
                        "BACHELOR OF ARTS IN JEWELRY DESIGN",
                        "BACHELOR OF ARTS IN MASS COMMUNICATION",
                        "BACHELOR OF ARTS IN PUBLIC RELATIONS",
                        "BACHELOR OF ARTS IN SOCIOLOGY",
                        "BACHELOR OF DENTAL SURGERY",
                        "BACHELOR OF EDUCATION IN EARLY CHILDHOOD",
                        "BACHELOR OF MEDICINE AND BACHELOR OF SURGERY",
                        "BACHELOR OF PHARMACY",
                        "BACHELOR OF SCIENCE IN ACCOUNTING",
                        "BACHELOR OF SCIENCE IN APPLIED PHYSICS",
                        "BACHELOR OF SCIENCE IN ARCHITECTURAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN BIOTECHNOLOGY",
                        "BACHELOR OF SCIENCE IN BUSINESS ADMINSTRATION",
                        "BACHELOR OF SCIENCE IN BUSINESS INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN CHEMISTRY",
                        "BACHELOR OF SCIENCE IN CIVIL ENGINEERING",
                        "BACHELOR OF SCIENCE IN CLINICAL NUTRITION AND DIETETICS",
                        "BACHELOR OF SCIENCE IN COMPUTER ENGINEERING",
                        "BACHELOR OF SCIENCE IN COMPUTER SCIENCE",
                        "BACHELOR OF SCIENCE IN ELECTRICAL AND ELECTRONICS ENGINEERING",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL HEALTH SCIENCES",
                        "BACHELOR OF SCIENCE IN FINANCE",
                        "BACHELOR OF SCIENCE IN HEALTH SERVICES ADMINISTRATION",
                        "BACHELOR OF SCIENCE IN INDUSTRIAL ENGINEERING AND ENGINEERING MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY - MULTIMEDIA",
                        "BACHELOR OF SCIENCE IN MANAGEMENT INFORMATION SYSTEMS",
                        "BACHELOR OF SCIENCE IN MATHEMATICS",
                        "BACHELOR OF SCIENCE IN MECHANICAL ENGINEERING",
                        "BACHELOR OF SCIENCE IN MEDICAL DIAGNOSTIC IMAGING",
                        "BACHELOR OF SCIENCE IN MEDICAL LABORATORY SCIENCES",
                        "BACHELOR OF SCIENCE IN NUCLEAR ENGINEERING",
                        "BACHELOR OF SCIENCE IN NURSING",
                        "BACHELOR OF SCIENCE IN PETROLEUM GEOSCIENCES AND REMOTE SENSING",
                        "BACHELOR OF SCIENCE IN PHYSIOTHERAPY",
                        "BACHELOR OF SCIENCE IN PUBLIC ADMINISTRATION",
                        "BACHELOR OF SCIENCE IN SUPPLY CHAIN MANAGEMENT",
                        "BACHELOR OF SCIENCE IN SUSTAINABLE AND RENEWABLE ENERGY ENGINEERING",
                        "BACHELOR OF SHARI'A (JURISPRUDENCE AND ITS FUNDAMENTALS)",
                        "BACHELOR OF SHARI'A - FOUNDATIONS OF RELIGION",
                        "BACHELOR OF SHARI'A AND LAW",
                        "BACHELOR OF SHARIAH"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF BUSINESS ADMINISTRATION",
                        "DOCTOR OF PHILOSOPHY IN APPLIED SOCIOLOGY",
                        "DOCTOR OF PHILOSOPHY IN ARABIC LANGUAGE AND LITERATURE",
                        "DOCTOR OF PHILOSOPHY IN CIVIL ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN COMMUNICATION",
                        "DOCTOR OF PHILOSOPHY IN ELECTRICAL AND COMPUTER ENGINEERING",
                        "DOCTOR OF PHILOSOPHY IN ENGINEERING MANAGEMENT",
                        "DOCTOR OF PHILOSOPHY IN EXEGESIS AND QURAN SCIENCES",
                        "DOCTOR OF PHILOSOPHY IN HADITH AND ITS SCIENCES",
                        "DOCTOR OF PHILOSOPHY IN HISTORY AND ISLAMIC CIVILIZATION",
                        "DOCTOR OF PHILOSOPHY IN JURISPRUDENCE AND ITS FOUNDATIONS",
                        "DOCTOR OF PHILOSOPHY IN LAW- PRIVATE LAW",
                        "DOCTOR OF PHILOSOPHY IN LAW- PUBLIC LAW",
                        "DOCTOR OF PHILOSOPHY IN LINGUISTICS AND TRANSLATION",
                        "DOCTOR OF PHILOSOPHY IN MOLECULAR MEDICINE AND TRANSLATIONAL RESEARCH"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "DOCTOR OF PHARMACY ",
                        "EXECUTIVE MASTER IN BUSINESS ADMINISTRATION",
                        "MASTER IN AIR AND SPACE LAW",
                        "MASTER IN CONSERVATION MANAGEMENT OF CULTURAL HERITAGE",
                        "MASTER IN EXEGESIS AND HADITH",
                        "MASTER IN JURISPRUDENCE AND ITS FOUNDATIONS",
                        "MASTER IN PHARMACEUTICAL SCIENCES",
                        "MASTER IN PRIVATE LAW",
                        "MASTER IN PUBLIC LAW",
                        "MASTER OF ARTS IN APPLIED SOCIOLOGY",
                        "MASTER OF ARTS IN ARABIC LANGUAGE AND LITERATURE",
                        "MASTER OF ARTS IN COMMUNICATION",
                        "MASTER OF ARTS IN HISTORY AND ISLAMIC CIVILIZATION",
                        "MASTER OF ARTS IN TRANSLATION",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF DENTAL SURGERY IN ENDODONTICS",
                        "MASTER OF DENTAL SURGERY IN ORAL SURGERY",
                        "MASTER OF DENTAL SURGERY IN PERIODONTOLOGY",
                        "MASTER OF DENTAL SURGERY IN PROSTHODONTICS",
                        "MASTER OF SCIENCE IN ADULT CRITICAL CARE NURSING",
                        "MASTER OF SCIENCE IN ASTRONOMY AND SPACE SCIENCES",
                        "MASTER OF SCIENCE IN BIOTECHNOLOGY",
                        "MASTER OF SCIENCE IN CHEMISTRY",
                        "MASTER OF SCIENCE IN CIVIL ENGINEERING",
                        "MASTER OF SCIENCE IN COMPUTER ENGINEERING",
                        "MASTER OF SCIENCE IN COMPUTER SCIENCE",
                        "MASTER OF SCIENCE IN DIABETES MANAGEMENT",
                        "MASTER OF SCIENCE IN ELECTRICAL AND ELECTRONICS ENGINEERING",
                        "MASTER OF SCIENCE IN ENGINEERING MANAGEMENT",
                        "MASTER OF SCIENCE IN ENVIRONMENTAL HEALTH",
                        "MASTER OF SCIENCE IN ENVIRONMENTAL SCIENCE AND ENGINEERING",
                        "MASTER OF SCIENCE IN GEOGRAPHIC INFORMATION SYSTEMS AND REMOTE SENSING",
                        "MASTER OF SCIENCE IN LEADERSHIP IN HEALTH PROFESSIONS EDUCATION",
                        "MASTER OF SCIENCE IN MEDICAL LABORATORY SCIENCES",
                        "MASTER OF SCIENCE IN MOLECULAR MEDICINE AND TRANSLATIONAL RESEARCH",
                        "MASTER OF SCIENCE IN PHYSICS",
                        "MASTER OF SCIENCE IN PHYSIOTHERAPY"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "POSTGRADUATE DIPLOMA IN ULTRASOUND TECHNOLOGY APPLICATIONS"
                    ]
                },
                {
                    "level": "GRADUATE DIPLOMA",
                    "fields": [
                        "PROFESSIONAL DIPLOMA IN TEACHING"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF STRATHCLYDE BUSINESS SCHOOL- UAE",
            "levels": [
                {
                    "level": "MASTER",
                    "fields": [
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF SCIENCE IN MARKETING",
                        "MASTER OF SCIENCE IN SUPPLY CHAIN AND LOGISTICS MANAGEMENT",
                        "MASTERS IN ENTREPRENEURSHIP"
                    ]
                }
            ]
        },
        {
            "institute": "UNIVERSITY OF WOLLONGONG IN DUBAI",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF BUSINESS ADMINISTRATION",
                        "BACHELOR OF BUSINESS INFORMATION SYSTEMS",
                        "BACHELOR OF COMMERCE IN ACCOUNTING",
                        "BACHELOR OF COMMERCE IN FINANCE",
                        "BACHELOR OF COMMERCE IN HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF COMMERCE IN INTERNATIONAL BUSINESS",
                        "BACHELOR OF COMMERCE IN MANAGEMENT",
                        "BACHELOR OF COMMERCE IN MARKETING",
                        "BACHELOR OF COMMUNICATION AND MEDIA",
                        "BACHELOR OF COMPUTER SCIENCE",
                        "BACHELOR OF ENGINEERING IN CIVIL ENGINEERING",
                        "BACHELOR OF ENGINEERING IN COMPUTER ENGINEERING",
                        "BACHELOR OF ENGINEERING IN ELECTRICAL ENGINEERING",
                        "BACHELOR OF ENGINEERING IN MECHANICAL ENGINEERING",
                        "BACHELOR OF ENGINEERING IN TELECOMMUNICATION ENGINEERING",
                        "BACHELOR OF NURSING"
                    ]
                },
                {
                    "level": "DOCTORATE",
                    "fields": [
                        "DOCTOR OF BUSINESS ADMINISTRATION",
                        "DOCTOR OF PHILOSOPHY IN BUSINESS"
                    ]
                },
                {
                    "level": "POSTGRADUATE CERTIFICATE",
                    "fields": [
                        "GRADUATE CERTIFICATE IN BUSINESS",
                        "GRADUATE CERTIFICATE IN ENGINEERING ASSET MANAEMENT",
                        "GRADUATE CERTIFICATE IN HUMAN RESOURCE MANAGEMENT",
                        "GRADUATE CERTIFICATE IN MARKETING"
                    ]
                },
                {
                    "level": "POSTGRADUATE DIPLOMA",
                    "fields": [
                        "GRADUATE DIPLOMA IN EDUCATIONAL STUDIES"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "GLOBAL EXECUTIVE MASTER OF LUXURY MANAGEMENT",
                        "MASTER OF APPLIED FINANCE",
                        "MASTER OF BUSINESS",
                        "MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER OF BUSINESS RESEARCH",
                        "MASTER OF EDUCATIONAL STUDIES",
                        "MASTER OF ENGINEERING ASSET MANAGEMENT",
                        "MASTER OF ENGINEERING MANAGEMENT",
                        "MASTER OF INFORMATION TECHNOLOGY MANAGEMENT",
                        "MASTER OF INTERNATIONAL RELATIONS",
                        "MASTER OF MEDIA AND COMMUNICATION",
                        "MASTER OF NURSING",
                        "MASTER OF QUALITY MANAGEMENT",
                        "MASTER OF SCIENCE IN LOGISTICS AND SUPPLY CHAIN MANAGEMENT"
                    ]
                }
            ]
        },
        {
            "institute": "ZAYED II MILITARY COLLEGE",
            "levels": [
                {
                    "level": "HIGHER DIPLOMA",
                    "fields": [
                        "HIGHER DIPLOMA IN MILITARY SCIENCES"
                    ]
                }
            ]
        },
        {
            "institute": "ZAYED UNIVERSITY",
            "levels": [
                {
                    "level": "BACHELOR",
                    "fields": [
                        "BACHELOR OF ARCHITECTURE",
                        "BACHELOR OF ARTS IN INTERNATIONAL STUDIES",
                        "BACHELOR OF FINE ARTS IN ANIMATION DESIGN",
                        "BACHELOR OF FINE ARTS IN GRAPHIC DESIGN",
                        "BACHELOR OF FINE ARTS IN INTERIOR DESIGN",
                        "BACHELOR OF FINE ARTS IN VISUAL ARTS",
                        "BACHELOR OF SCIENCE IN ACCOUNTING",
                        "BACHELOR OF SCIENCE IN COMMUNICATION AND MEDIA SCIENCES",
                        "BACHELOR OF SCIENCE IN EDUCATION",
                        "BACHELOR OF SCIENCE IN ENVIRONMENTAL SCIENCE AND SUSTAINABILITY",
                        "BACHELOR OF SCIENCE IN FINANCE",
                        "BACHELOR OF SCIENCE IN HUMAN RESOURCE MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INFORMATION SYSTEMS AND TECHNOLOGY MANAGEMENT",
                        "BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY",
                        "BACHELOR OF SCIENCE IN MARKETING AND ENTREPRENEURSHIP",
                        "BACHELOR OF SCIENCE IN MULTIMEDIA DESIGN",
                        "BACHELOR OF SCIENCE IN PSYCHOLOGY",
                        "BACHELOR OF SCIENCE IN PUBLIC HEALTH AND NUTRITION"
                    ]
                },
                {
                    "level": "MASTER",
                    "fields": [
                        "EXECUTIVE MASTER OF BUSINESS ADMINISTRATION",
                        "MASTER IN DIPLOMACY AND INTERNATIONAL AFFAIRS",
                        "MASTER OF ARTS IN COMMUNICATION",
                        "MASTER OF EDUCATION IN EDUCATIONAL LEADERSHIP AND ADMINISTRATION",
                        "MASTER OF EDUCATION IN TEACHING AND LEARNING",
                        "MASTER OF ISLAMIC STUDIES IN JURISPRUDENCE OF REALITY",
                        "MASTER OF LEGAL AND JUDICIAL STUDIES",
                        "MASTER OF SCIENCE IN FINANCE",
                        "MASTER OF SCIENCE IN INFORMATION SYSTEMS MANAGEMENT",
                        "MASTER OF SCIENCE IN INFORMATION TECHNOLOGY"
                    ]
                }
            ]
        }
    ]
