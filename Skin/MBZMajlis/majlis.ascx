<%@ Control language="cs" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>

<!--#include file="Common/Common.ascx"-->

<div class="container">
<!--#include file="Common/ProfileLink.ascx"-->
<!--#include file="Common/Menu.ascx"-->
    <main class="maincontent">
        <!--  Eighth section   -->
        <section class="majlis-live-section" id="the-majlis">
           <div class="live-video-left-content">
                <div class="section-header">
                    <p class="section-header-nav-title"><%= GetString("TheMajlis") %></p>
                    <p class="section-header-sub-nav--title">/<%= GetString("Live") %></p>
                </div>
                <div class="section-live-block">
                    <div class="section-title">
                        <h2>
                            <%= GetString("MOHAMEDBINZAYEDMAJLISFORFUTUREGENERATIONS") %> <br/>
                            <span><%= GetString("Date") %></span>
                        </h2>
                    </div>
                    <div id="ContentPane" runat="server" class="live-video-block"></div>
                    <div id="LoginPane" runat="server" style="display:none;"></div>
                    <div id="RegisterPane" runat="server" style="display:none;"></div>
                    <div id="ThankYouPane" runat="server" class="thankyouPart"></div>
                </div>
           </div>
            <div class="live-video-right-content">
                <div class="majlis-right-sidebar-stiky">
                    <div class="majlis-right-sidebar">
                        <div class="live-agenda" id="open-agenda-modal">
                            <p><%= GetString("Agenda") %></p>
                        </div>
                    </div>

                    <div class="live-agenda-modal hide-modal">
                        <div class="live-agenda-modal-header" id="close-modal">
                            <img src="<%=SkinPath%>assets/arrow.svg">
                        </div>
                        <div class="live-agenda-modal-body">
                            <h2><%= GetString("Agenda") %></h2>
                            <div id="AjendaPane" runat="server"></div>

                            <h3><%= GetString("HAPPENINGNOW") %></h3>
                            <div id="HappeningPane" runat="server"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--  End Eighth section   -->
    </main>
</div>
